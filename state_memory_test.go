package dynamap

import (
	"testing"
	"time"
)

func NewTestProcess(id string, engine *BPMNEngine) ProcessInstance {
	startEvent := NewNoneStartEvent(id, "", "Start", engine.IdGenerator.GenerateId(NoneStartEventType))
	pi := ProcessInstance{Id: id, Key: "TestKey", Version: "1", BpmnEngine: engine, Status: ElementCreated, Impl: &TestProcess{}, StartElement: startEvent}
	return pi
}

type TestProcess struct {
}

func (p *TestProcess) GetNextElement(engine *BPMNEngine, g IdGenerator, t *Token, currentElement ElementStateProvider) ElementStateProvider {
	if currentElement == nil {
		startId := g.GenerateId(NoneStartEventType)
		currentElement = NewNoneStartEvent(t.ProcessInstance.Id, t.Id, "Start", startId)
	}
	key := currentElement.GetElementState().Key
	switch key {
	case "Start":
		id := g.GenerateId(NoneIntermediateThrowEventType)
		return NewNoneIntermediateThrowEvent(t.ProcessInstance.Id, t.Id, "ThrowEvent1", id)
	case "ThrowEvent1":
		id := g.GenerateId(UserTaskType)
		return NewUserTask[BaseTask](t.ProcessInstance.Id, t.Id, "Task1", id, "", true)
	case "Task1":
		id := g.GenerateId(NoneIntermediateThrowEventType)
		return NewNoneIntermediateThrowEvent(t.ProcessInstance.Id, t.Id, "ThrowEvent2", id)
	case "ThrowEvent2":
		id := g.GenerateId(NoneEndEventType)
		return NewNoneEndEvent(t.ProcessInstance.Id, t.Id, "EndEvent_1", id, true)
	case "EndEvent_1":
		return nil
	}
	return nil
}

func autoCompleteTasks(engine *BPMNEngine) {
	// Wait for process instances to start
	for {
		piCmdResp, _ := (*engine).SendCommand(GetProcessInstancesCmd{})
		piResp := piCmdResp.(GetProcessInstancesResp)
		if piResp.Data != nil && len(piResp.Data) > 0 {
			break
		}
		time.Sleep(1 * time.Millisecond)
	}

	// Complete tasks
	for {
		time.Sleep(1 * time.Millisecond)
		piCmdResp, _ := (*engine).SendCommand(GetProcessInstancesCmd{})
		piResp := piCmdResp.(GetProcessInstancesResp)
		if piResp.Data == nil || len(piResp.Data) == 0 {
			return
		}
		taskCmdResp, _ := (*engine).SendCommand(GetTasksCmd{})
		taskResp := taskCmdResp.(GetTasksResp)
		for _, t := range taskResp.Data {
			(*engine).SendCommand(CompleteUserTasksCmd{Tasks: []CompleteUserTaskCmd{{
				ProcessInstanceId: t.ProcessInstanceId,
				TaskId:            t.Id,
				Data: BaseTask{
					CompletedBy: CompletedBy{
						CompletedById:   "User1",
						CompletedByUser: "Fred",
					}},
			}}})
		}
	}
}

func TestStateEntryEquals(t *testing.T) {
	one := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	two := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	if one != two {
		t.Fatalf("%v != %v, expected ==", one, two)
	}
}

func TestStateEntryNotEquals(t *testing.T) {
	one := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	two := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "2",
		Status:      ElementActivating,
	}
	if one == two {
		t.Fatalf("%v == %v, expected !=", one, two)
	}
}

func TestStoreProcessInstanceMemoryStateSingle(t *testing.T) {
	var store ProcessInstanceStore
	store = NewMemoryStateStore()
	state := ProcessInstanceState{
		Id:      "1",
		Key:     "Process",
		Version: "1",
		Status:  ElementActivating,
	}
	store.WriteProcessInstanceState(state)
	states, err := store.ReadProcessInstances()
	if err != nil {
		t.Fatalf("got err %v, expected state.", err)
	}

	if len(states) != 1 {
		t.Fatalf("got %v, expected 1", len(states))
	}

	if states[0] != state {
		t.Fatalf("got %v, expected %v", states[0], state)
	}
}

func TestMemoryStateStoreInterface(t *testing.T) {
	var s StateStore
	s = NewMemoryStateStore()
	_, ok := s.(*MemoryStateStore)
	if !ok {
		t.Fatalf("MemoryStateStore does not implement StateStore")
	}
}

func TestStoreProcessInstanceMemoryStateReplace(t *testing.T) {
	var store ProcessInstanceStore
	store = NewMemoryStateStore()
	state := ProcessInstanceState{
		Id:      "1",
		Key:     "Process",
		Version: "1",
		Status:  ElementActivating,
	}
	store.WriteProcessInstanceState(state)
	state.Status = ElementActivated
	states, _ := store.ReadProcessInstances()
	got := states[0].Status
	want := ElementActivating
	if got != want {
		t.Fatalf("Got '%v', want '%v'", got, want)
	}
	store.WriteProcessInstanceState(state)
	states, _ = store.ReadProcessInstances()
	got = states[0].Status
	want = ElementActivated
	if got != want {
		t.Fatalf("Got '%v', want '%v'", got, want)
	}
}

func TestProcessInstanceExecuteStateCompletedInStore(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	engine.StateStore.(*MemoryStateStore).StoreCompleted = true
	if err := engine.Start(); err != nil {
		t.Fatalf("unexpected error starting engine: %v", err)
	}
	id := "P1"
	p := NewTestProcess(id, engine)
	_, err := engine.SendCommand(StartProcessInstanceCmd{&p})
	if err != nil {
		t.Fatalf("unexpected error starting process: %v", err)
	}
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	gotState, err := engine.ProcessInstanceStore.ReadProcessInstance(id)
	wantState := ProcessInstanceState{
		Id:     id,
		Key:    "TestKey",
		Status: ElementCompleted,
	}
	if err != nil || gotState != wantState {
		t.Fatalf("got err %v %+v, expected %+v", err, gotState, wantState)
	}
}

func TestProcessInstanceExecuteStateCompletedNotInStore(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	if err := engine.Start(); err != nil {
		t.Fatalf("unexpected error starting engine: %v", err)
	}
	id := "P1"
	p := NewTestProcess(id, engine)
	_, err := engine.SendCommand(StartProcessInstanceCmd{&p})
	if err != nil {
		t.Fatalf("unexpected error starting process: %v", err)
	}
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()

	// Check no process instances remaining
	pis, err := engine.StateStore.ReadProcessInstances()
	if err != nil {
		t.Fatalf("unexpected error %v", err)
	}
	got := len(pis)
	want := 0
	if got != want {
		t.Errorf("got %d process instances, want %d", got, want)
	}

	// Check no tokens remaining
	tokens, err := engine.StateStore.ReadTokens()
	if err != nil {
		t.Fatalf("unexpected error %v", err)
	}
	got = len(tokens)
	want = 0
	if got != want {
		t.Errorf("got %d tokens, want %d", got, want)
	}

	// Check no states remaining
	states, err := engine.StateStore.ReadAllElementStates()
	if err != nil {
		t.Fatalf("unexpected error %v", err)
	}
	got = len(states)
	want = 0
	if got != want {
		t.Errorf("got %d states, want %d", got, want)
	}
}

func TestMemoryStateStore_WriteToken(t *testing.T) {
	store := NewMemoryStateStore()
	pi := ProcessInstanceState{Id: "1"}
	store.WriteProcessInstanceState(pi)
	token := TokenState{
		Id:                "12345",
		ProcessInstanceId: "1",
		CurrentElementId:  "92",
		Complete:          false,
	}
	err := store.WriteToken(token)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err := store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen := len(tokens)
	wantLen := 1
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
	got := tokens[0]
	want := token
	if got != want {
		t.Fatalf("Got token '%+v', want '%+v'", got, want)
	}

	// Write second token
	token2 := TokenState{
		Id:                "12346",
		ProcessInstanceId: "1",
		CurrentElementId:  "98",
		Complete:          false,
	}
	err = store.WriteToken(token2)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 2
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
	got = tokens[1]
	if got.Id != "12346" {
		got = tokens[0]
	}
	want = token2
	if got != want {
		t.Fatalf("Got token '%+v', want '%+v'", got, want)
	}

	// Complete token 1
	token.Complete = true
	err = store.WriteToken(token)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 2
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}

	// Complete token 2
	token2.Complete = true
	err = store.WriteToken(token2)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 2
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}

	for _, token := range tokens {
		if !token.Complete {
			t.Errorf("token '%+v' should be complete", token)
		}
	}
}

func TestStoreTaskChannel(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	store := engine.StateStore.(*MemoryStateStore)
	store.StoreCompleted = true
	if err := engine.Start(); err != nil {
		t.Fatalf("unexpected error starting engine: %v", err)
	}

	// Subscribe to element state changes
	sid, ch := store.SubscribeElementState()

	// Start Process
	id := "P1"
	p := NewTestProcess(id, engine)
	_, err := engine.SendCommand(StartProcessInstanceCmd{&p})
	if err != nil {
		t.Fatalf("unexpected error starting process: %v", err)
	}

	// Listen for tasks to be completed and complete them
	for taskState := range ch {
		if taskState.ElementType == UserTaskType && taskState.Status == ElementActivated {
			(*engine).SendCommand(CompleteUserTasksCmd{Tasks: []CompleteUserTaskCmd{{
				ProcessInstanceId: taskState.ProcessInstanceId,
				TaskId:            taskState.Id,
				Data: BaseTask{
					CompletedBy: CompletedBy{
						CompletedById:   "User1",
						CompletedByUser: "Fred",
					}},
			}}})
		} else if taskState.ElementType == NoneEndEventType {
			break
		}
	}
	store.UnsubscribeElementState(sid)
	engine.Wait()
	engine.Stop()

	gotState, err := engine.ProcessInstanceStore.ReadProcessInstance(id)
	wantState := ProcessInstanceState{
		Id:     id,
		Key:    "TestKey",
		Status: ElementCompleted,
	}
	if err != nil || gotState != wantState {
		t.Fatalf("got err %v %+v, expected %+v", err, gotState, wantState)
	}
}

func TestPrimarySecondaryStoreProcessInstance(t *testing.T) {
	var p, s1, s2 StateStore
	p = NewMemoryStateStore()
	s1 = NewMemoryStateStore()
	s2 = NewMemoryStateStore()
	ps := NewPrimarySecondaryStateStore(p, []StateStore{s1, s2})
	info := ProcessInstanceState{Id: "1", Status: ElementActivating}
	err := ps.WriteProcessInstanceState(info)
	if err != nil {
		t.Errorf("write process instance error: %v", err)
	}

	// Primary
	pInfo, err := p.ReadProcessInstance("1")
	if err != nil {
		t.Errorf("read primary store process instance error: %v", err)
	}
	got := pInfo.Id
	want := info.Id
	if got != want {
		t.Errorf("read primary store process instance: got '%s', want '%s'", got, want)
	}

	// S1
	s1Info, err := s1.ReadProcessInstance("1")
	got = s1Info.Id
	want = info.Id
	if got != want {
		t.Errorf("read secondary store 1 process instance: got '%s', want '%s'", got, want)
	}

	// S2
	s2Info, err := s2.ReadProcessInstance("1")
	got = s2Info.Id
	want = info.Id
	if got != want {
		t.Errorf("read secondary store 2 process instance: got '%s', want '%s'", got, want)
	}

	// Test local change to pInfo doesn't affect store (e.g. stored by value, not reference)
	info.Status = ElementActivated
	pInfo, err = p.ReadProcessInstance("1")
	if err != nil {
		t.Errorf("read primary store process instance error: %v", err)
	}
	got = pInfo.Status
	want = ElementActivating
	if got != want {
		t.Errorf("read primary store process instance: got '%s', want '%s'", got, want)
	}

	// Store new status in primary store, and verify reads from primarysecondary store come from primary store
	info.Status = ElementActivated
	p.WriteProcessInstanceState(info)
	pInfo, err = ps.ReadProcessInstance("1")
	got = pInfo.Status
	want = ElementActivated
	if got != want {
		t.Errorf("read primary store process instance: got '%s', want '%s'", got, want)
	}
}

func TestPrimarySecondaryStoreToken(t *testing.T) {
	var p, s1, s2 StateStore
	p = NewMemoryStateStore()
	s1 = NewMemoryStateStore()
	s2 = NewMemoryStateStore()
	ps := NewPrimarySecondaryStateStore(p, []StateStore{s1, s2})
	ps.WriteProcessInstanceState(ProcessInstanceState{Id: "2"})
	info := TokenState{Id: "1", ProcessInstanceId: "2", CurrentElementId: "3"}
	err := ps.WriteToken(info)
	if err != nil {
		t.Errorf("write token error: %v", err)
	}

	// Primary
	pInfo, err := p.ReadTokens()
	if err != nil {
		t.Errorf("read primary store token error: %v", err)
	}
	got := pInfo[0].Id
	want := info.Id
	if got != want {
		t.Errorf("read primary store token: got '%s', want '%s'", got, want)
	}

	// S1
	s1Info, err := s1.ReadTokens()
	got = s1Info[0].Id
	want = info.Id
	if got != want {
		t.Errorf("read secondary store 1 token: got '%s', want '%s'", got, want)
	}

	// S2
	s2Info, err := s2.ReadTokens()
	got = s2Info[0].Id
	want = info.Id
	if got != want {
		t.Errorf("read secondary store 2 token: got '%s', want '%s'", got, want)
	}

	// Todo: Test mutating primary store, and test read from primary secondary store
}

func TestPrimarySecondaryStoreElementReadStates(t *testing.T) {
	var p, s1, s2 StateStore
	p = NewMemoryStateStore()
	s1 = NewMemoryStateStore()
	s2 = NewMemoryStateStore()
	ps := NewPrimarySecondaryStateStore(p, []StateStore{s1, s2})
	err := ps.WriteProcessInstanceState(ProcessInstanceState{Id: "2"})
	if err != nil {
		t.Errorf("write process instance error: %v", err)
	}
	token := TokenState{Id: "1", ProcessInstanceId: "2", CurrentElementId: "3"}
	err = ps.WriteToken(token)
	if err != nil {
		t.Errorf("write token error: %v", err)
	}
	element := ElementState{Id: "3", ProcessInstanceId: "2", TokenId: "1"}
	err = ps.WriteElement(element)
	if err != nil {
		t.Errorf("write element state error: %v", err)
	}

	// Primary
	eInfo, err := p.ReadAllElementStates()
	if err != nil {
		t.Errorf("read primary store element error: %v", err)
	}
	got := eInfo[0].Id
	want := element.Id
	if got != want {
		t.Errorf("read primary store element state: got '%s', want '%s'", got, want)
	}

	// S1
	s1Info, err := s1.ReadAllElementStates()
	got = s1Info[0].Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 1 element state: got '%s', want '%s'", got, want)
	}

	// S2
	s2Info, err := s2.ReadAllElementStates()
	got = s2Info[0].Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 2 element state: got '%s', want '%s'", got, want)
	}

	// Todo: Test mutating primary store, and test read from primary secondary store
}

func TestPrimarySecondaryStoreElementReadState(t *testing.T) {
	var p, s1, s2 StateStore
	p = NewMemoryStateStore()
	s1 = NewMemoryStateStore()
	s2 = NewMemoryStateStore()
	ps := NewPrimarySecondaryStateStore(p, []StateStore{s1, s2})
	err := ps.WriteProcessInstanceState(ProcessInstanceState{Id: "2"})
	if err != nil {
		t.Errorf("write process instance error: %v", err)
	}
	token := TokenState{Id: "1", ProcessInstanceId: "2", CurrentElementId: "3"}
	err = ps.WriteToken(token)
	if err != nil {
		t.Errorf("write token error: %v", err)
	}
	element := ElementState{Id: "3", ProcessInstanceId: "2", TokenId: "1", ElementType: UserTaskType}
	err = ps.WriteElement(element)

	if err != nil {
		t.Errorf("write element state error: %v", err)
	}

	// Primary
	eInfo, err := p.ReadElementState(UserTaskType, "3")
	if err != nil {
		t.Errorf("read primary store element error: %v", err)
	}
	got := eInfo.Id
	want := element.Id
	if got != want {
		t.Errorf("read primary store element state: got '%s', want '%s'", got, want)
	}

	// S1
	s1Info, err := s1.ReadElementState(UserTaskType, "3")
	got = s1Info.Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 1 element state: got '%s', want '%s'", got, want)
	}

	// S2
	s2Info, err := s2.ReadElementState(UserTaskType, "3")
	got = s2Info.Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 2 element state: got '%s', want '%s'", got, want)
	}

	// Todo: Test mutating primary store, and test read from primary secondary store
}

func TestPrimarySecondaryStoreReadElementStates(t *testing.T) {
	var p, s1, s2 StateStore
	p = NewMemoryStateStore()
	s1 = NewMemoryStateStore()
	s2 = NewMemoryStateStore()
	ps := NewPrimarySecondaryStateStore(p, []StateStore{s1, s2})
	err := ps.WriteProcessInstanceState(ProcessInstanceState{Id: "2"})
	if err != nil {
		t.Errorf("write process instance error: %v", err)
	}
	token := TokenState{Id: "1", ProcessInstanceId: "2", CurrentElementId: "3"}
	err = ps.WriteToken(token)
	if err != nil {
		t.Errorf("write token error: %v", err)
	}
	element := ElementState{Id: "3", ProcessInstanceId: "2", TokenId: "1", ElementType: UserTaskType, Key: "Test"}
	err = ps.WriteElement(element)

	if err != nil {
		t.Errorf("write element state error: %v", err)
	}

	// Primary
	eInfo, err := p.ReadElementStates(UserTaskType, "Test")
	if err != nil {
		t.Errorf("read primary store element error: %v", err)
	}
	got := eInfo[0].Id
	want := element.Id
	if got != want {
		t.Errorf("read primary store element state: got '%s', want '%s'", got, want)
	}

	// S1
	s1Info, err := s1.ReadElementStates(UserTaskType, "Test")
	got = s1Info[0].Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 1 element state: got '%s', want '%s'", got, want)
	}

	// S2
	s2Info, err := s2.ReadElementStates(UserTaskType, "Test")
	got = s2Info[0].Id
	want = element.Id
	if got != want {
		t.Errorf("read secondary store 2 element state: got '%s', want '%s'", got, want)
	}

	// Todo: Test mutating primary store, and test read from primary secondary store
}

/*
func TestProcessInstanceMemoryStateHistoryStore(t *testing.T) {
	var engine BPMNEngine
	engine = NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	p := NewTestProcess(id, &engine)
	engine.SendCommand(StartProcessInstanceCmd{Instance: &p})
	autoCompleteTasks(&engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType ElementType, gotStates []StateEntry, PKey, id string, index int, status ElementState) {
		wantState := StateEntry{
			ElementType: elementType,
			Key:         PKey,
			Id:          id,
			Status:      status,
		}
		if gotStates[index] != wantState {
			t.Fatalf("got %v, expected %v", gotStates[index], wantState)
		}
	}
	piStates, _ := engine.StateHistoryStore().getStateHistory(ProcessInstanceType, id)
	checkState(ProcessInstanceType, piStates, "", id, 0, ElementActivating)
	checkState(ProcessInstanceType, piStates, "", id, 1, ElementActivated)
	checkState(ProcessInstanceType, piStates, "", id, 2, ElementCompleting)
	checkState(ProcessInstanceType, piStates, "", id, 3, ElementCompleted)

	startStates, _ := engine.StateHistoryStore().getStateHistory(NoneStartEventType, "0")
	checkState(NoneStartEventType, startStates, "Start", "0", 0, ElementActivating)
	checkState(NoneStartEventType, startStates, "Start", "0", 1, ElementActivated)
	checkState(NoneStartEventType, startStates, "Start", "0", 2, ElementCompleting)
	checkState(NoneStartEventType, startStates, "Start", "0", 3, ElementCompleted)

	if len(startStates) != 4 {
		t.Fatalf("got %v start states, wanted 4", len(startStates))
	}

	event1States, _ := engine.StateHistoryStore().getStateHistory(NoneIntermediateThrowEventType, "1")
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 0, ElementActivating)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 1, ElementActivated)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 2, ElementCompleting)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 3, ElementCompleted)

	if len(event1States) != 4 {
		t.Fatalf("got %v intermediate throw event states, wanted 4", len(startStates))
	}

	endStates, _ := engine.StateHistoryStore().getStateHistory(NoneEndEventType, "4")
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 0, ElementActivating)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 1, ElementActivated)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 2, ElementCompleting)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 3, ElementCompleted)

	if len(endStates) != 4 {
		t.Fatalf("got %v end event states, wanted 4", len(endStates))
	}
}
*/
