package dynamap

import (
	"context"
	"testing"
)

func TestNoneStartEventActivated(t *testing.T) {
	e := NewNoneStartEvent("", "", "Start", "123")
	got := e.Status
	want := ElementActivated
	if got != want {
		t.Errorf("got '%s', want '%s'", got, want)
	}
}

func TestNoneStartEventCompleted(t *testing.T) {
	e := NewNoneStartEvent("", "", "Start", "123")
	ctx := context.Background()
	engine := NewMemoryBPMNEngine()
	e.RunLifecycle(ctx, engine, nil)
	got := e.Status
	want := ElementCompleted
	if got != want {
		t.Errorf("got '%s', want '%s'", got, want)
	}
}
