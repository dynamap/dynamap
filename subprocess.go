package dynamap

import (
	"context"
	"log"
	"strconv"
)

type SubProcess struct {
	ElementState
	NumberOfInstances int
	StartElementKey   string
	Tokens            []*Token
	BpmnEngine        *BPMNEngine
	getNextElement    func(engine *BPMNEngine, g IdGenerator, t *Token, currentElement ElementStateProvider, loopCounter int) ElementStateProvider
}

func NewSubProcess(pid, tid, key, id string, engine *BPMNEngine, g IdGenerator, startElementKey string, numberOfInstances int, getNextElement func(engine *BPMNEngine, g IdGenerator, t *Token, currentElement ElementStateProvider, loopCounter int) ElementStateProvider) *SubProcess {
	tokens := make([]*Token, 0)
	return &SubProcess{
		ElementState:      ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: SubProcessType},
		NumberOfInstances: numberOfInstances,
		getNextElement:    getNextElement,
		StartElementKey:   startElementKey,
		Tokens:            tokens,
		BpmnEngine:        engine,
	}
}

func (sp *SubProcess) GetElementState() ElementState {
	return sp.ElementState
}

func (sp *SubProcess) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	sp.Activated(ctx, bpmnEngine, token)
	sp.Completed(ctx, bpmnEngine, token)
	return nil
}

func (sp *SubProcess) Activated(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	sp.Status = ElementActivated
	if sp.Store {
		bpmnEngine.WriteElement(sp.ElementState)
	}
	sp.ExecuteProcesses(ctx, bpmnEngine, token)
}

func (sp *SubProcess) ExecuteProcesses(ctx context.Context, bpmnEngine *BPMNEngine, parentToken *Token) {
	g := bpmnEngine.IdGenerator
	for i := 0; i < sp.NumberOfInstances; i++ {
		seId := g.GenerateId(NoneStartEventType)
		tid := strconv.Itoa(i)
		p := parentToken.ProcessInstance
		se := NewNoneStartEvent(sp.ProcessInstanceId, tid, sp.StartElementKey, seId)
		t := NewToken(se, p, p.BpmnEngine)
		t.Id = tid
		for t.CurrentElement != nil {
			t.CurrentElement = sp.getNextElement(p.BpmnEngine, p.BpmnEngine.IdGenerator, t, t.CurrentElement, i)
			if t.CurrentElement == nil {
				break
			}
			currentLifeCycleRunner, ok := t.CurrentElement.(LifeCycleRunner)
			if !ok {
				log.Printf("sub process instance run: cannot convert element %+v to LifeCycleRunner", currentLifeCycleRunner)
				break
			}
			currentLifeCycleRunner.RunLifecycle(ctx, t.BpmnEngine, t)
		}
	}
}

func (sp *SubProcess) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	sp.Status = ElementCompleted
	if sp.Store {
		bpmnEngine.WriteElement(sp.ElementState)
	}
}
