module cron

go 1.22.0

require (
	github.com/robfig/cron/v3 v3.0.1
	gitlab.com/dynamap/dynamap v0.8.0
)

replace gitlab.com/dynamap/dynamap => ../../../dynamap
