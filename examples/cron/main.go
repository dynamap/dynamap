package main

import (
	"context"
	"log"
	"os"
	"os/signal"


	"gitlab.com/dynamap/dynamap"

)


type Data struct {
	Msg string
}


func cronProcess(ctx context.Context) {
	// create and start a processor
	var err error
	engine := dynamap.NewMemoryBPMNEngine()
	if err = engine.Start(); err != nil {
		log.Fatal(err)
	}

	var tes TimerEventStarter
	cc := NewCronClock()
	err = tes.StartEventProcessesWithClock(ctx, engine, cc)
	if err != nil {
		log.Printf("StartEvent error %v", err)
	}
	cc.Start()
	log.Printf("waiting for engine to complete!\n")
}

func WaitForInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<- c
}

func main() {
	println("starting...")

	appCtx, appCtxCancel := context.WithCancel(context.Background())

	cronProcess(appCtx)

	WaitForInterrupt()

	appCtxCancel()

}


