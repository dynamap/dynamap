package main

import (
	"github.com/robfig/cron/v3"
)

type CronClock struct {
	cr *cron.Cron
}

func NewCronClock() *CronClock {
	return &CronClock{
		cr: cron.New(),
	}
}

func (cc *CronClock) SpecHandler(spec string, handler func()) error {
	_, err := cc.cr.AddFunc(spec, handler)
	return err
}

func (cc *CronClock) Start() {
	cc.cr.Start()
}

func (cc *CronClock) Stop() {
	cc.cr.Stop()
}
