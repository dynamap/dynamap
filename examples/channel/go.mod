module wf

go 1.22.0

require (
	gitlab.com/dynamap/dynamap v0.9.1
	gitlab.com/quanteksystems/rodynamics-go-common v0.1.9
)

replace gitlab.com/dynamap/dynamap => ../../../dynamap
