package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/dynamap/dynamap"
	"gitlab.com/quanteksystems/rodynamics-go-common/util"
)

type Data struct {
	Val int
	C   chan int
}

func main() {

	appCtx, appCancel := context.WithCancel(context.Background())

	engine := dynamap.NewMemoryBPMNEngineWithContext(appCtx)

	c := make(chan *Data, 1)
	instanceStarter := dynamap.NewInstanceStarter(engine, NewTestProcess, "StartEvent_1")
	cf := dynamap.StartInstanceWithChannel(appCtx, c, instanceStarter)

	respChan := make(chan int)

	// lets wait to see what we get back

	go func() {
		for {
			select {
			case <-appCtx.Done():
				return
			case i := <-respChan:
				log.Printf("got back : %d", i)
			}
		}
	}()

	// send some ticks
	val := 1
	go func() {
		log.Printf("starting ticker")
		for {
			select {
			case <-appCtx.Done():
				log.Printf("ticker dying")
				return
			case <-time.After(2 * time.Second):
				log.Printf("tick!")
				c <- &Data{
					Val: val,
					C:   respChan,
				}
				val++
			}
		}
	}()

	engine.AddStartEventProcessor(cf)

	if err := engine.Start(); err != nil {
		log.Printf("Start engine error %v", err)
		return
	}

	log.Printf("Waiting for ^C")
	util.WaitForInterrupt()
	appCancel()
	log.Printf("waiting for shutdown!")
	time.Sleep(5 * time.Second)
}
