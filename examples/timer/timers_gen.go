package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	Flow_0dbzirm = "Flow_0dbzirm"

	Flow_09zjrsb = "Flow_09zjrsb"

	Flow_14w2uav = "Flow_14w2uav"

	Flow_0k3vpcu = "Flow_0k3vpcu"

	StartEvent = "StartEvent"

	Event_0s91icb = "Event_0s91icb"

	Gateway_0comnha = "Gateway_0comnha"

	Activity_1wtuir5 = "Activity_1wtuir5"

	Activity_0osm63m = "Activity_0osm63m"
)

type TimersProcess struct {
	mu   sync.RWMutex
	data *Data

	// callback block
	start func()
	done  func()
}

func NewTimersProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data *Data) *dynamap.ProcessInstance {
	impl := &TimersProcess{
		//			mu: sync.RWMutex{},
		data:  data,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "TimersProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *TimersProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.data
}

type TimerEventStarterOption func(*TimerEventStarterConfig)

type TimerEventStarterConfig struct {
	newData           func() *Data
	OverrideTimerSpec string
}

func NewTimerEventStarterConfig() *TimerEventStarterConfig {
	return &TimerEventStarterConfig{
		newData: func() *Data { return &Data{} },
	}
}

func WithTimerEventStarterData(f func() *Data) TimerEventStarterOption {
	return func(cfg *TimerEventStarterConfig) {
		cfg.newData = f
	}
}

func WithOverrideTimerSpec(ots string) TimerEventStarterOption {
	return func(cfg *TimerEventStarterConfig) {
		cfg.OverrideTimerSpec = ots
	}
}

type TimerEventStarter struct {
	cfg        *TimerEventStarterConfig
	timerCycle time.Duration
}

func (p *TimerEventStarter) TimerSpec() string {
	return `R/PT2S`
}

func (tes *TimerEventStarter) ParseAndValidateCycleSpec(spec string) error {
	d, err := dynamap.NewISO8601DateTime(spec)
	if err != nil {
		return err
	}
	dur, err := d.Duration.Duration()
	if err != nil {
		return err
	}
	tes.timerCycle = dur
	return nil
}

func (tes *TimerEventStarter) TimeAfter() <-chan time.Time {
	return time.After(tes.timerCycle)
}

func (tes *TimerEventStarter) TickAction(ctx context.Context, b *dynamap.BPMNEngine) {
	pid := b.IdGenerator.GenerateId(dynamap.ProcessInstanceType)
	startEvent := dynamap.NewTimerStartEvent(pid, "", "StartEvent", b.IdGenerator.GenerateId(dynamap.TimerStartEventType))
	//	timerProcess := NewTimersProcess(pid, b, startEvent, &Data{})
	timerProcess := NewTimersProcess(pid, b, startEvent, tes.cfg.newData())
	b.SendCommand(dynamap.StartProcessInstanceCmd{Instance: timerProcess})
}

// This must implement the StartEventProcessor interface from engine.go
func (tes *TimerEventStarter) StartEventProcesses(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, opts ...any) error {
	var delay time.Duration
	tes.cfg = NewTimerEventStarterConfig()
	for _, opt := range opts {
		switch o := opt.(type) {
		case TimerEventStarterOption:
			o(tes.cfg)
		case dynamap.TimerDelayOpt:
			delay = time.Duration(o)
		}
	}
	timerSpec := tes.TimerSpec()
	if len(tes.cfg.OverrideTimerSpec) != 0 {
		timerSpec = tes.cfg.OverrideTimerSpec
	}
	err := tes.ParseAndValidateCycleSpec(timerSpec)
	if err != nil {
		return err
	}

	delayChan := make(chan struct{})
	if delay < 0 {
		delay = 0
	}
	if delay > 0 {
		go func() {
			time.Sleep(delay)
			close(delayChan)
		}()
	}
	if delay == 0 {
		close(delayChan)
	}
	go func() {
		// fire once at startup to avoid delays for longer cycles
		<-delayChan
		tes.TickAction(ctx, bpmnEngine)
		for {
			select {
			case <-ctx.Done():
				return
			case <-tes.TimeAfter():
				tes.TickAction(ctx, bpmnEngine)

			}
		}
	}()
	return nil
}

func (tes *TimerEventStarter) StartEventProcessesWithClock(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, clk dynamap.ClockSource, opts ...any) error {

	tes.cfg = NewTimerEventStarterConfig()
	for _, opt := range opts {
		switch o := opt.(type) {
		case TimerEventStarterOption:
			o(tes.cfg)
		}
	}

	tickChan := make(chan time.Time, 1)

	timerSpec := tes.TimerSpec()
	if len(tes.cfg.OverrideTimerSpec) != 0 {
		timerSpec = tes.cfg.OverrideTimerSpec
	}

	err := clk.SpecHandler(timerSpec, func() {
		tickChan <- time.Now()
	})
	if err != nil {
		log.Printf("[StartEventProcessesWithClock] bad spec %s error %v", timerSpec, err)
		return err
	}

	// TODO(FEI) - we shouldn't really be doing this
	clk.Start()

	go func() {
		for {
			select {
			case <-ctx.Done():
				clk.Stop()
				close(tickChan)
				return
			case <-tickChan:
				tes.TickAction(ctx, bpmnEngine)

			}
		}
	}()
	return nil
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *TimersProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case Flow_0dbzirm:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			log.Printf("Task1 %+v", p.data)
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, Activity_1wtuir5, id, script)

	case Flow_09zjrsb:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_14w2uav, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, Gateway_0comnha, id, gwHandler)

	case Flow_14w2uav:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			log.Printf("Task2")
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, Activity_0osm63m, id, script)

	case Flow_0k3vpcu:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, Event_0s91icb, id, true)

	case StartEvent:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0dbzirm, id)

	case Event_0s91icb:
		return nil

	case Gateway_0comnha:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case Activity_1wtuir5:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_09zjrsb, id)

	case Activity_0osm63m:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0k3vpcu, id)

	}
	return nil
}
