package main

import (
	"context"
	"log"
	"os"
	"os/signal"

	"gitlab.com/dynamap/dynamap"

)

type Data struct {
	Msg string
}

func clockedProcess(ctx context.Context) {

	// create and start a processor
	engine := dynamap.NewMemoryBPMNEngine()
	if err := engine.Start(); err != nil {
		log.Fatal(err)
	}

	// create a timer starter
	var tes TimerEventStarter	

	// start timer with local data
	err := tes.StartEventProcesses(ctx, engine,
				WithTimerEventStarterData(
					func() *Data {
						return &Data{Msg: "what!"}
					}))

	if err != nil {
		log.Printf("StartEventProcess error %v", err)
		return
	}

	log.Printf("timer event started ^C to kill")
	
}

func WaitForInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<- c
}

func main() {
	println("starting...")

	appCtx, appCtxCancel := context.WithCancel(context.Background())

	clockedProcess(appCtx)

	WaitForInterrupt()

	appCtxCancel()

}


