package dynamap

import (
	"context"
	"strconv"
	"testing"
)

func TestIDGenerator(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	g := NewMemoryIdGenerator()
	g.Start(ctx)
	for i := 0; i <= 50; i++ {
		id := g.GenerateId(NoneStartEventType)
		if strconv.Itoa(i+1) != id {
			t.Fatalf("Got %v, want %v", id, i)
		}
	}
}
