package dynamap

import (
	"context"
	"log"
	"sync"
	"time"
)

type TokenState struct {
	Id                string
	ProcessInstanceId string
	CurrentElementId  string
	Complete          bool
}

type Token struct {
	Id                          string
	PrevElement, CurrentElement ElementStateProvider
	BpmnEngine                  *BPMNEngine
	ProcessInstance             *ProcessInstance
	mu                          sync.RWMutex
	acceptingCommands           bool
	complete                    bool
	cmdReq                      chan any
	cmdResp                     chan any
}

func NewToken(startElement ElementStateProvider, processInstance *ProcessInstance, engine *BPMNEngine) *Token {
	return &Token{
		Id:                engine.GenerateId(TokenType),
		PrevElement:       nil,
		CurrentElement:    startElement,
		BpmnEngine:        engine,
		ProcessInstance:   processInstance,
		complete:          false,
		acceptingCommands: false,
		cmdReq:            make(chan any),
		cmdResp:           make(chan any),
	}
}

func (t *Token) CurrentState() ElementStateProvider {
	return t.CurrentElement
}

func (t *Token) PreviousState() ElementStateProvider {
	return t.PrevElement
}

func (t *Token) AcceptingCommands() bool {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.acceptingCommands
}

func (t *Token) setAcceptingCommands(accept bool) {
	t.mu.Lock()
	defer t.mu.Unlock()
	t.acceptingCommands = accept
}

func (t *Token) Run(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	t.WriteState()

	for {
		if t.CurrentElement != nil && t.CurrentElement.GetElementState().Status == ElementActivated {
			// Element is already active, typically when token state loaded from store on startup
			currentLifeCycleRunner, ok := t.CurrentElement.(LifeCycleRunner)
			if !ok {
				log.Printf("token: cannot convert element %+v to LifeCycleRunner", t.CurrentElement)
				t.CurrentElement = nil
				break
			}
			err := currentLifeCycleRunner.RunLifecycle(ctx, t.BpmnEngine, t)
			if err != nil {
				// Todo: Specifically check for cancellation/shutdown
				break
			}
		}

		nextElement := t.ProcessInstance.psm.GetNextElement(t.BpmnEngine, t.BpmnEngine.IdGenerator, t, t.CurrentElement)
		t.PrevElement = t.CurrentElement
		t.CurrentElement = nextElement
		if t.CurrentElement == nil {
			// Need to set complete before writing state
			t.mu.Lock()
			t.complete = true
			t.mu.Unlock()
		}
		t.WriteState()
		if t.CurrentElement == nil {
			break
		}
		currentLifeCycleRunner, ok := t.CurrentElement.(LifeCycleRunner)
		if !ok {
			log.Printf("token: cannot convert element %+v to LifeCycleRunner", t.CurrentElement)
			t.CurrentElement = nil
			break
		}
		err := currentLifeCycleRunner.RunLifecycle(ctx, t.BpmnEngine, t)
		if err != nil {
			// Todo: Specifically check for cancellation/shutdown
			break
		}
	}
	close(t.cmdResp)
}

func (t *Token) WriteState() {
	t.mu.RLock()
	var currentElementId string
	if t.CurrentElement != nil {
		currentElementId = t.CurrentElement.GetElementState().Id
	}
	info := TokenState{
		Id:                t.Id,
		ProcessInstanceId: t.ProcessInstance.Id,
		CurrentElementId:  currentElementId,
		Complete:          t.complete,
	}
	t.mu.RUnlock()
	t.BpmnEngine.WriteToken(info)
}

func (t *Token) SendCommand(ctx context.Context, cmd any, timeout time.Duration) any {
	if t.IsComplete() || !t.AcceptingCommands() {
		return nil // Todo: Return an error
	}
	select {
	case <-ctx.Done():
		return nil // Todo: Return an error
	case t.cmdReq <- cmd:
		return <-t.cmdResp
	case <-time.After(timeout):
		return nil // Todo: Return an error
	}
}

func (t *Token) IsComplete() bool {
	t.mu.RLock()
	defer t.mu.RUnlock()
	return t.complete
}
