package dynamap

import "context"

type NoneIntermediateThrowEvent struct {
	ElementState
}

func NewNoneIntermediateThrowEvent(pid, tid, key, id string) *NoneIntermediateThrowEvent {
	return &NoneIntermediateThrowEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneIntermediateThrowEventType}}
}

func (e *NoneIntermediateThrowEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneIntermediateThrowEvent) RefElement() any {
	return e
}

func (e *NoneIntermediateThrowEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	if e.Store {
		bpmnEngine.WriteElement(e.ElementState)
	}
	return nil
}
