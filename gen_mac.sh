./bin/mac/dynamap -input "./internal/plan/plan.bpmn" -output "./internal/plan/plan_gen.go" gen
./bin/mac/dynamap -input "./internal/risk/risk.bpmn" -output "./internal/risk/risk_gen.go" gen
./bin/mac/dynamap -input "./internal/timer/timer.bpmn" -output "./internal/timer/timer_gen.go" gen
./bin/mac/dynamap -input "./internal/noop/noop.bpmn" -output "./internal/noop/noop_gen.go"
./bin/mac/dynamap -input "./internal/subprocess/subprocess.bpmn" -output "./internal/subprocess/subprocess_gen.go"
