package dynamap

import (
	"errors"
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ErrStoreElementNotFound = NewBPMNEngineError(nil, "element not found")
)

const (
	DefaultSubscriberTimeout         = 10 * time.Second // default timeout to consider a subscriber slow
	DefaultSubscriberMaxTimeoutCount = 10               // default max number of timeouts occurred before a subscriber is removed
)

// ProcessInstanceCreator creates a new process instance in a store
type ProcessInstanceCreator interface {
	// CreateProcessInstance creates a new process instance in the store.
	// If the process instance does not already have an id, it is generated
	// by the store and returned as a string. If there is an error, the
	// id will be empty and an error returned.
	CreateProcessInstance(state ProcessInstanceState) (string, error)
}

// ProcessInstanceWriter writes the state of a process to a store
type ProcessInstanceWriter interface {
	WriteProcessInstanceState(state ProcessInstanceState) error
}

// ProcessInstanceReader reads the state of process instance(s)
type ProcessInstanceReader interface {
	ReadProcessInstance(id string) (ProcessInstanceState, error) // Get rid of?
	ReadProcessInstances() ([]ProcessInstanceState, error)
}

// ProcessInstanceStore can read and write process instances
type ProcessInstanceStore interface {
	ProcessInstanceCreator
	ProcessInstanceReader
	ProcessInstanceWriter
}

// TokenWriter writes the state of a token to a store
type TokenWriter interface {
	WriteToken(state TokenState) error
}

// TokenReader reads the state of a token from a store
type TokenReader interface {
	ReadTokens() ([]TokenState, error)
}

// TokenStore can read and write tokens
type TokenStore interface {
	TokenReader
	TokenWriter
}

// ElementWriter writes a StateEntry
type ElementWriter interface {
	// WriteElement writes an element state
	WriteElement(state ElementState) error
}

type ElementStateSubscriber interface {
	SubscribeElementState() (uint64, <-chan ElementState)
	UnsubscribeElementState(uint64)
}

// ElementReader reads a StateEntry based on filters
type ElementReader interface {
	// ReadElementState returns the current state for a given elementType and id.
	ReadElementState(elementType string, id string) (ElementState, error)
	// ReadElementStates returns all states in the store for a specific element type and key
	ReadElementStates(elementType string, key string) ([]ElementState, error)
	// ReadAllElementStates returns all states in the store
	ReadAllElementStates() ([]ElementState, error)
}

// StateStore combines a state reader and writer
type StateStore interface {
	ProcessInstanceCreator
	ProcessInstanceWriter
	ProcessInstanceReader
	TokenWriter
	TokenReader
	ElementWriter
	ElementReader
	ElementStateSubscriber
}

// NewMemoryStateStore creates a new in memory state store
func NewMemoryStateStore() *MemoryStateStore {
	store := MemoryStateStore{}
	store.processInstances = make(map[string]MemoryProcessInstanceStore)
	store.tasks = make(map[string]string)
	store.elementSubscribers = make(map[uint64]*MemoryStateStoreElementSubscriber, 0)
	store.IdGenerator = NewMemoryIdGenerator()
	return &store
}

// PrimarySecondaryStateStore stores runtime data in the primary store,
// and replicates data to secondary stores. Read operations occur from the primary store.
type PrimarySecondaryStateStore struct {
	Primary   StateStore
	Secondary []StateStore
}

// SubscribeElementState subscribes to element state changes in the primary store.
func (s *PrimarySecondaryStateStore) SubscribeElementState() (uint64, <-chan ElementState) {
	return s.Primary.SubscribeElementState()
}

// UnsubscribeElementState unsubscribes an element state change subscriber from the primary store.
func (s *PrimarySecondaryStateStore) UnsubscribeElementState(u uint64) {
	s.Primary.UnsubscribeElementState(u)
}

// NewPrimarySecondaryStateStore creates a new composite store that stores runtime data in the primary store,
// and replicates data to secondary stores. Read operations occur from the primary store.
func NewPrimarySecondaryStateStore(primary StateStore, secondary []StateStore) *PrimarySecondaryStateStore {
	return &PrimarySecondaryStateStore{primary, secondary}
}

func (s *PrimarySecondaryStateStore) CreateProcessInstance(state ProcessInstanceState) (string, error) {
	errs := make([]error, 0)
	id, err := s.Primary.CreateProcessInstance(state)
	if err != nil {
		return "", err
	}
	state.Id = id
	for i := range s.Secondary {
		sid, err := s.Secondary[i].CreateProcessInstance(state)
		if err != nil {
			errs = append(errs, err)
		}
		if sid != id {
			errs = append(errs, fmt.Errorf("secondary store created new process instance id '%s', expected primary id '%s'", sid, id))
		}
	}
	return id, errors.Join(errs...)
}

// WriteProcessInstanceState writes process instance state to the primary store and secondary stores.
func (s *PrimarySecondaryStateStore) WriteProcessInstanceState(state ProcessInstanceState) error {
	errs := make([]error, 0)
	err := s.Primary.WriteProcessInstanceState(state)
	if err != nil {
		errs = append(errs, err)
	}
	for i := range s.Secondary {
		err := s.Secondary[i].WriteProcessInstanceState(state)
		if err != nil {
			errs = append(errs, err)
		}
	}
	return errors.Join(errs...)
}

// ReadProcessInstance reads process instance state from the primary store by id
func (s *PrimarySecondaryStateStore) ReadProcessInstance(id string) (ProcessInstanceState, error) {
	return s.Primary.ReadProcessInstance(id)
}

// ReadProcessInstances reads all process instance state from the primary store
func (s *PrimarySecondaryStateStore) ReadProcessInstances() ([]ProcessInstanceState, error) {
	return s.Primary.ReadProcessInstances()
}

// WriteToken writes token state to the primary and secondary stores
func (s *PrimarySecondaryStateStore) WriteToken(state TokenState) error {
	errs := make([]error, 0)
	err := s.Primary.WriteToken(state)
	if err != nil {
		errs = append(errs, err)
	}
	for i := range s.Secondary {
		err := s.Secondary[i].WriteToken(state)
		if err != nil {
			errs = append(errs, err)
		}
	}
	return errors.Join(errs...)
}

// ReadTokens reads all tokens from the primary store
func (s *PrimarySecondaryStateStore) ReadTokens() ([]TokenState, error) {
	return s.Primary.ReadTokens()
}

// WriteElement writes element state to the primary and secondary stores
func (s *PrimarySecondaryStateStore) WriteElement(state ElementState) error {
	errs := make([]error, 0)
	err := s.Primary.WriteElement(state)
	if err != nil {
		errs = append(errs, err)
	}
	for i := range s.Secondary {
		err := s.Secondary[i].WriteElement(state)
		if err != nil {
			errs = append(errs, err)
		}
	}
	return errors.Join(errs...)
}

// ReadElementState returns the current state from the primary store for a given elementType and id.
func (s *PrimarySecondaryStateStore) ReadElementState(elementType string, id string) (ElementState, error) {
	return s.Primary.ReadElementState(elementType, id)
}

// ReadElementStates returns all states in the primary store for a specific element type and key
func (s *PrimarySecondaryStateStore) ReadElementStates(elementType string, key string) ([]ElementState, error) {
	return s.Primary.ReadElementStates(elementType, key)
}

// ReadAllElementStates returns all states in the primary store
func (s *PrimarySecondaryStateStore) ReadAllElementStates() ([]ElementState, error) {
	return s.Primary.ReadAllElementStates()
}

// MemoryProcessInstanceStore stores the state of process instances and their tokens
type MemoryProcessInstanceStore struct {
	state  ProcessInstanceState        // stores state for the process
	tokens map[string]MemoryTokenStore // stores tokens for the process
}

// MemoryTokenStore stores the state of tokens
type MemoryTokenStore struct {
	state       TokenState                 // contains the token state
	statesById  map[string]ElementState    // stores StateEntries based on the element id
	statesByKey map[string][]*ElementState // stores StateEntries based on the element key
}

// MemoryStateStoreElementSubscriber stores communication information about a subscriber to element state changes
type MemoryStateStoreElementSubscriber struct {
	id           uint64            // unique id of subscriber
	ch           chan ElementState // channel to send element state changes to the subscriber
	timeoutCount int               // count of timeouts to the subscriber
}

// MemoryStateStore stores process instance, token, and element states in memory
type MemoryStateStore struct {
	processInstances   map[string]MemoryProcessInstanceStore         // stores process instances by id
	tasks              map[string]string                             // stores tasks
	elementSubscribers map[uint64]*MemoryStateStoreElementSubscriber // channel for notifying element changes
	IdGenerator        IdGenerator                                   // generates ids for process instances, tokens, and elements
	gsid               uint64                                        // global subscriber id generator
	mu                 sync.Mutex                                    // synchronizes access to data
	muSub              sync.Mutex                                    // synchronizes access to subscribers
	StoreCompleted     bool                                          // flag indicating whether completed process instances, tokens, and elements should be removed when complete.
}

func (m *MemoryStateStore) CreateProcessInstance(state ProcessInstanceState) (string, error) {
	if state.Id == "" {
		state.Id = m.IdGenerator.GenerateId(ProcessInstanceType)
	}

	m.mu.Lock()
	defer m.mu.Unlock()
	m.processInstances[state.Id] = MemoryProcessInstanceStore{
		state:  state,
		tokens: make(map[string]MemoryTokenStore),
	}

	return state.Id, nil
}

// WriteProcessInstanceState writes the state of a process instance. Does not include tokens or element states.
func (m *MemoryStateStore) WriteProcessInstanceState(state ProcessInstanceState) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Delete if completed and flag set to not store
	if !m.StoreCompleted && state.Status == ElementCompleted {
		delete(m.processInstances, state.Id)
		return nil
	}

	// Store state
	pi, ok := m.processInstances[state.Id]
	if !ok {
		m.processInstances[state.Id] = MemoryProcessInstanceStore{
			state:  state,
			tokens: make(map[string]MemoryTokenStore),
		}
	} else {
		pi.state = state
		m.processInstances[state.Id] = pi
	}

	return nil
}

// ReadProcessInstance reads the state of a process instance. Does not include tokens or element states.
func (m *MemoryStateStore) ReadProcessInstance(id string) (ProcessInstanceState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	pi, ok := m.processInstances[id]
	if !ok {
		return pi.state, errors.New(fmt.Sprintf("process instance with id %s not found", id))
	}
	return pi.state, nil
}

// WriteToken writes the state of a token
func (m *MemoryStateStore) WriteToken(state TokenState) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Get Process Instance
	pi, ok := m.processInstances[state.ProcessInstanceId]
	if !ok {
		return errors.New(fmt.Sprintf("process instance with id %s not found", state.ProcessInstanceId))
	}

	// Get Token
	t, ok := pi.tokens[state.Id]
	if !ok {
		t = MemoryTokenStore{
			state:       state,
			statesById:  make(map[string]ElementState),
			statesByKey: make(map[string][]*ElementState),
		}
	}
	t.state = state
	pi.tokens[state.Id] = t

	return nil
}

// ReadTokens reads the state of all tokens
func (m *MemoryStateStore) ReadTokens() ([]TokenState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	tokens := make([]TokenState, 0)
	for _, pi := range m.processInstances {
		for _, token := range pi.tokens {
			tokens = append(tokens, token.state)
		}
	}
	return tokens, nil
}

// ReadProcessInstances reads the state of all process instances. Does not include tokens or element states.
func (m *MemoryStateStore) ReadProcessInstances() ([]ProcessInstanceState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	pis := make([]ProcessInstanceState, len(m.processInstances))
	i := 0
	for _, pi := range m.processInstances {
		pis[i] = pi.state
		i++
	}
	return pis, nil
}

func (m *MemoryStateStore) ProcessInstanceCount() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return len(m.processInstances)
}

// WriteElement writes an element's state
func (m *MemoryStateStore) WriteElement(state ElementState) error {
	id := getStateID(state.ElementType, state.Id)
	key := getStateKey(state.ElementType, state.Key)
	m.mu.Lock()
	{
		// Get Process Instance
		pi, ok := m.processInstances[state.ProcessInstanceId]
		if !ok {
			m.mu.Unlock()
			return errors.New(fmt.Sprintf("process instance with id %s not found", state.ProcessInstanceId))
		}

		// Get Token
		t, ok := pi.tokens[state.TokenId]
		if !ok {
			m.mu.Unlock()
			return errors.New(fmt.Sprintf("token with id %s not found", state.TokenId))
		}

		// Store by Id
		t.statesById[id] = state

		// Store by key with pointer
		keyStates, ok := t.statesByKey[key]
		if !ok {
			keyStates = make([]*ElementState, 0)
		}
		keyStates = append(keyStates, &state)
		t.statesByKey[key] = keyStates
		go m.notifySubscribers(state)
	}
	m.mu.Unlock()
	// Todo: Need to write memory states in the context of their parent/process
	if state.Object == nil {
		return nil
	}
	/*switch o := state.Object.(type) {
	case ElementState:
		//fmt.Printf("IElement RefElement: %T\n", o.RefElement())
		switch e := o.Object.(type) {
		case *UserTask[DefaultTask]:
			//fmt.Println("Got User Task!")
			result, _ := json.Marshal(e.completed)
			m.mu.Lock()
			m.tasks[id] = string(result)
			m.mu.Unlock()
			//fmt.Printf("Completed By: %+v\n", e.CompletedBy)
		}
	case ProcessInstance:
		//fmt.Println("ProcessInstanceState")
	}*/
	return nil
}

// ReadElementState returns the current state for a given elementType and id.
func (m *MemoryStateStore) ReadElementState(elementType string, id string) (ElementState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	var e ElementState
	found := false
Loop:
	for _, pi := range m.processInstances {
		for _, t := range pi.tokens {
			state, ok := t.statesById[getStateID(elementType, id)]
			if ok {
				e = state
				found = true
				break Loop
			}
		}
	}

	if !found {
		return e, ErrStoreElementNotFound
	}
	return e, nil
}

// ReadElementStates returns all states in the store for a specific element type and key
func (m *MemoryStateStore) ReadElementStates(elementType string, key string) ([]ElementState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	states := make([]ElementState, 0)
	for _, pi := range m.processInstances {
		for _, t := range pi.tokens {
			keyStates, ok := t.statesByKey[getStateKey(elementType, key)]
			if ok {
				for _, s := range keyStates {
					states = append(states, *s)
				}
			}
		}
	}

	return states, nil
}

// ReadAllElementStates returns all states in the store
func (m *MemoryStateStore) ReadAllElementStates() ([]ElementState, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	states := make([]ElementState, 0)
	for _, pi := range m.processInstances {
		for _, t := range pi.tokens {
			for _, state := range t.statesById {
				states = append(states, state)
			}
		}
	}
	return states, nil
}

// SubscribeElementState creates a new subscription for element changes and returns a new id for the subscriber
// and a channel to receive notifications. The id is needed to call UnsubscribeElementState.
func (m *MemoryStateStore) SubscribeElementState() (uint64, <-chan ElementState) {
	s := MemoryStateStoreElementSubscriber{
		id:           atomic.AddUint64(&m.gsid, 1),
		ch:           make(chan ElementState, 1),
		timeoutCount: 0,
	}
	m.muSub.Lock()
	m.elementSubscribers[s.id] = &s
	m.muSub.Unlock()
	return s.id, s.ch
}

// UnsubscribeElementState removes a subscriber
func (m *MemoryStateStore) UnsubscribeElementState(id uint64) {
	m.muSub.Lock()
	defer m.muSub.Unlock()
	delete(m.elementSubscribers, id)
}

// notifySubscribers notifies all elementSubscribers of an element state change. Timeouts after a default time period.
func (m *MemoryStateStore) notifySubscribers(state ElementState) {
	m.muSub.Lock()
	defer m.muSub.Unlock()
	delSubs := make([]uint64, 0)
	for i := range m.elementSubscribers {
		select {
		case m.elementSubscribers[i].ch <- state:
		case <-time.After(DefaultSubscriberTimeout):
			m.elementSubscribers[i].timeoutCount += 1
			if m.elementSubscribers[i].timeoutCount >= DefaultSubscriberMaxTimeoutCount {
				delSubs = append(delSubs, m.elementSubscribers[i].id)
			}
			fmt.Printf("memory state store: timeout writing state: %+v", state)
		}
	}

	for _, id := range delSubs {
		delete(m.elementSubscribers, id)
		fmt.Printf("memory state store: deleted slow subscriber %d", id)
	}
}

// getStateID returns a composite id composed of the elementType and id.
// This guards against if different types of elements having the same id.
// Ids should be unique within a given type of element.
func getStateID(elementType string, id string) string {
	return elementType + ":" + id
}

// getStateKey returns a composite key composed of the elementType and key.
// This guards against if different types of elements having the same key.
// Keys should be unique within a given type of element.
func getStateKey(elementType string, key string) string {
	return elementType + ":" + key
}
