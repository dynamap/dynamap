package dynamap

import "context"

type ScriptTask struct {
	ElementState
	script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)
}

func NewScriptTask(pid, tid, key, id string, script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)) *ScriptTask {
	return &ScriptTask{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ScriptTaskType}, script: script}
}

func (st *ScriptTask) GetElementState() ElementState {
	return st.ElementState
}

func (st *ScriptTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	st.Activated(ctx, bpmnEngine, token)
	st.Completed(ctx, bpmnEngine, token)
	return nil
}

func (st *ScriptTask) Activated(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementActivated
	if st.Store {
		bpmnEngine.WriteElement(st.ElementState)
	}
	st.script(ctx, bpmnEngine, token, st)
}

func (st *ScriptTask) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementCompleted
	if st.Store {
		bpmnEngine.WriteElement(st.ElementState)
	}
}
