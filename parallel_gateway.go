package dynamap

import (
	"context"
	"fmt"
	"time"
)

type ParallelGatewayCmd struct {
	pg ParallelGateway
}

type ParallelGateway struct {
	ElementState
	CurrentSeqKey string
	InSeqKeys     []string
	OutSeqKeys    []string
}

func NewParallelGateway(pid, tid, key, id, currentSeqKey string, inSeqKeys, outSeqKeys []string) *ParallelGateway {
	return &ParallelGateway{
		ElementState:  ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ParallelGatewayType},
		CurrentSeqKey: currentSeqKey,
		InSeqKeys:     inSeqKeys,
		OutSeqKeys:    outSeqKeys,
	}
}

func (pg *ParallelGateway) GetElementState() ElementState {
	return pg.ElementState
}

func (pg *ParallelGateway) RefElement() any {
	return pg
}

func (pg *ParallelGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	if err := pg.Completing(ctx, bpmnEngine, token); err != nil {
		return err
	}
	pg.Completed(ctx, bpmnEngine, token)
	return nil
}

func (pg *ParallelGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	pg.Status = ElementCompleting
	if pg.Store {
		bpmnEngine.WriteElement(pg.ElementState)
	}
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("parallel gateway %s: cancelling", pg.Key)
		default:
			resp, err := token.ProcessInstance.SendCommand(*pg)
			if err != nil {
				fmt.Printf("parallel gateway %s: %v\n", pg.Key, err)
				time.Sleep(1 * time.Millisecond)
				continue
			}
			done, ok := resp.(<-chan any)
			if !ok {
				fmt.Printf("parallel gateway: completing received: %v", done)
				time.Sleep(1 * time.Millisecond)
				continue
			}
			<-done
			return nil
		}
	}
	return nil
}

func (pg *ParallelGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	pg.Status = ElementCompleted
	if pg.Store {
		bpmnEngine.WriteElement(pg.ElementState)
	}
}
