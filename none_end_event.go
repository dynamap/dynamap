package dynamap

import "context"

type NoneEndEvent struct {
	ElementState
}

func NewNoneEndEvent(pid, tid, key, id string, store bool) *NoneEndEvent {
	return &NoneEndEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneEndEventType, Store: store}}
}

func (e *NoneEndEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneEndEvent) RefElement() any {
	return e
}

func (e *NoneEndEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	if e.Store {
		bpmnEngine.WriteElement(e.ElementState)
	}
	return nil
}
