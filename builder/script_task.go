package builder

import "log"

type ScriptProcessor struct {
}

func (sp ScriptProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.ScriptTasks {
		xe := xe
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "scriptTask", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "scriptTask", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (sp ScriptProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.ScriptTasks {
		GenerateNextStateCode(&e, process, sp, ps)
	}
}

func (sp ScriptProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return GenerateCodeForSingleOutgoingElement(currentElement, nextElements, process, ps)
}

func (sp ScriptProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	var xmlScript Script
	xmlScript, ok := newElement.XMLElement.(Script)
	if !ok {
		log.Fatalf("No script found for script task `%s`", newElement.Id)
	}
	code :=
		`id := g.GenerateId(dynamap.ScriptTaskType)
			script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {`
	code += xmlScript.GetScript()
	code += `
			}
			return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, ` + newElement.Id + `, id, script)`
	return code
}
