package builder

import (
	"errors"
	"fmt"
	"log"
	"strings"
)

type ParallelGatewayProcessor struct {
}

func (pp ParallelGatewayProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.ParallelGateways {
		xe := xe
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "parallelGateway", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "parallelGateway", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (pp ParallelGatewayProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.ParallelGateways {
		GenerateNextStateCode(&e, process, pp, ps)
	}
}

func (pp ParallelGatewayProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return "return nil"
}

func (pp ParallelGatewayProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	code := "id := g.GenerateId(dynamap.ParallelGatewayType)\n"
	incoming, err := getParallelGatewayIncomingKeys(newElement)
	if err != nil {
		log.Fatalf(err.Error())
	}
	outgoing, err := getParallelGatewayOutgoingKeys(newElement)
	if err != nil {
		log.Fatalf(err.Error())
	}
	code += "\t\t\tinSeqKeys := " + incoming + "\n"
	code += "\t\t\toutSeqKeys := " + outgoing + "\n"
	code += "\t\treturn dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id, " + currentElement.Id + ", inSeqKeys, outSeqKeys)"
	return code
}

func getParallelGatewayIncomingKeys(element *Element) (string, error) {
	var xmlIncoming Incoming
	xmlIncoming, ok := element.XMLElement.(Incoming)
	if !ok {
		return "", errors.New("no incoming elements on parallel gateway.")
	}
	incoming := make([]string, 0)
	for _, inc := range xmlIncoming.GetIncoming() {
		incoming = append(incoming, fmt.Sprintf("%s", inc))
	}
	return "[]string{" + strings.Join(incoming, ",") + "}", nil
}

func getParallelGatewayOutgoingKeys(element *Element) (string, error) {
	var xmlOutgoing Outgoing
	xmlOutgoing, ok := element.XMLElement.(Outgoing)
	if !ok {
		return "", errors.New("no outgoing elements on parallel gateway.")
	}
	outgoing := make([]string, 0)
	for _, out := range xmlOutgoing.GetOutgoing() {
		outgoing = append(outgoing, fmt.Sprintf("%s", out))
	}
	return "[]string{" + strings.Join(outgoing, ",") + "}", nil
}
