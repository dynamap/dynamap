package builder

// Todo: refactor to remove getUserTaskGoDataType from bpmn_generator, which shouldn't know anything about specific elements ideally. It should just invoke ElementProcessors

import (
	"bytes"
	_ "embed"
	"errors"
	"fmt"
	"go/format"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"golang.org/x/mod/modfile"
)

var _ = exec.Command

type Process struct {
	Id                  string
	Name                string
	StartEvent          *Element
	Elements            []*Element
	AllElements         []*Element
	SubProcesses        []*SubProcess
	UserTasks           []*Element
	elementMap          map[string]*Element
	allElementMap       map[string]*Element
	subProcessMap       map[string]*SubProcess
	GoModule            string
	GoPackage           string
	GoStartVariableName string
	GoStartVariableType string
}

type SubProcess struct {
	Id           string
	Name         string
	Elements     []*Element
	SubProcesses []*SubProcess
	elementMap   map[string]*Element
}

type Element struct {
	Id            string
	Name          string
	BpmnType      string
	XMLElement    any
	NextStateCode string
}

type ProcessCode struct {
	Process *Process
	Code    *string
}

type OutputDefProcessCode struct {
	InputFile  string // Input BPMN file path including file name
	OutputFile string // Output go code path including file name
	Defs       *XMLBPMNDefinitions
	OutputCode *ProcessCode // Output code
	PkgName    string
}

func NewProcessGenerator(input, output string) *ProcessGenerator {
	return &ProcessGenerator{
		Input:     input,
		Output:    output,
		OutputGen: make(map[string]*OutputDefProcessCode),
	}
}

// ProcessGenerator generates go code for a process
type ProcessGenerator struct {
	// Input BPMN file or path to process BPMN files
	Input string

	// Output BPMN file or path for generated code
	Output string

	// Generates the output file path/name for a given input file, and the input/output args.
	OutputFileNameGenerator func(inputFile string, process *XMLBPMNProcess, input, output string, isOutputDir bool) string

	// Stores output definitions and code given input file path and name
	OutputGen map[string]*OutputDefProcessCode

	ProcessTemplate string
	ProcessorSet    *ProcessorSet
	PackageName     string
}

func separateDirAndGoFile(name string) (string, string) {
	dirName := filepath.Dir(name)
	baseName := filepath.Base(name)
	ext := filepath.Ext(baseName)
	// is it an explicit go filename
	if ext == ".go" {
		return dirName, baseName
	}
	// it has an extension but it isn't a go file
	if len(ext) != 0 {
		return dirName, ""
	}
	// just given a directory
	return name, ""
}

func (p *ProcessGenerator) GenerateAndWriteCodeFromBPMN() error {
	// Get input path info
	inputFileInfo, err := os.Stat(p.Input)
	if err != nil {
		return err
	}

	// Get output path info
	outputDir, outputFilename := separateDirAndGoFile(p.Output)
	if len(outputDir) == 0 {
		outputDir = "."
	}
	/*
		outputFileInfo, err := os.Stat(b.Output)
		if err != nil {
			return err
		}
	*/

	// Get bpmn files to process
	var inputFileNames []string
	if inputFileInfo.IsDir() {
		/*
			// Input is a directory, ensure output is a directory
			if !outputFileInfo.IsDir() {
				return errors.New("output must be a directory since input is a directory. singular output file specified")
			}
		*/

		// Read all BPMN input files in directory
		files, err := filepath.Glob(filepath.Join(p.Input, "*.bpmn"))
		if err != nil {
			log.Fatal(err)
		}
		inputFileNames = append(inputFileNames, files...)

	} else {
		// Input is a single named file
		inputFileNames = append(inputFileNames, p.Input)
	}

	// Generate go code from bpmn files
	if len(inputFileNames) > 1 {
		// regardless of input always force generated
		outputFilename = ""
	}
	for _, inputFile := range inputFileNames {
		fmt.Printf("Processing file: %s\n", inputFile)

		// Parse definitions
		defs, err := parseBPMNModelFromFile(inputFile)
		if err != nil {
			return err
		}
		// override package name from command line option
		if len(p.PackageName) != 0 {
			defs.Process.GoPackage = p.PackageName
		}

		// Generate code
		pc, err := generateCode(defs, p.ProcessTemplate, p.ProcessorSet)
		if err != nil {
			return err
		}

		// Determine output file location and name
		outputFile := p.Output
		if p.OutputFileNameGenerator != nil {
			outputFile = p.OutputFileNameGenerator(inputFile, &defs.Process, p.Input, outputDir, true)
		} else {
			inputFileName := filepath.Base(inputFile)
			if len(outputFilename) == 0 {
				// no outputfilename given - construct from input filename
				outputFilename = inputFileName[:len(inputFileName)-len(filepath.Ext(inputFileName))] + "_gen.go"
			}
			outputFile = path.Join(outputDir, outputFilename)
		}

		// Write output file
		err = os.WriteFile(outputFile, []byte(*pc.Code), 0666)
		if err != nil {
			fmt.Printf("Error writing file: %v\n", err)
			continue
		}

		// Create and store outputGen
		outputGen := OutputDefProcessCode{InputFile: inputFile, OutputFile: outputFile, Defs: defs, OutputCode: pc}
		p.OutputGen[inputFile] = &outputGen

		// Format/update imports
		fmt.Printf("Output file: %s\n", outputFile)
		cmd := exec.Command("goimports", "-w", outputFile)
		err = cmd.Run()
		if err != nil {
			return err
		}
		fmt.Printf("Format/Imports file: %s\n", outputFile)
	}
	return err
}

func generateProcess(defs *XMLBPMNDefinitions, ps *ProcessorSet) (*Process, error) {
	setGoDefaults(defs)

	// Create new process
	process := Process{
		Id:                  defs.Process.Id,
		Name:                defs.Process.Name,
		GoModule:            defs.Process.GoModule,
		GoPackage:           defs.Process.GoPackage,
		GoStartVariableName: defs.Process.GoStartVariableName,
		GoStartVariableType: defs.Process.GoStartVariableType,
	}
	if err := validateProcessMeta(process); err != nil {
		return &process, err
	}
	process.elementMap = make(map[string]*Element)
	process.allElementMap = make(map[string]*Element)
	process.subProcessMap = make(map[string]*SubProcess)

	CreateElements(&process, nil, nil, &defs.Process.XMLBPMNElements, ps)
	GenerateCode(&process, &defs.Process.XMLBPMNElements, ps)

	return &process, nil
}

// CreateElements executes the element processors to create elements based on the XML BPMN elements.
// The elements are used for code generattion.
func CreateElements(process *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, ep := range ps.ElementProcessors {
		err := ep.CreateElements(process, subprocess, spElements, elements, ps)
		if err != nil {
			return err
		}
	}
	return nil
}

// setGoDefaults sets the default go module, package, and start variable name/type for code generation
func setGoDefaults(defs *XMLBPMNDefinitions) {
	processId := strings.ToLower(defs.Process.Id)
	if defs.Process.GoModule == "" {
		name, err := GetModuleName("go.mod")
		if err != nil {
			fmt.Printf("generator go defaults: %v\n", err)
			defs.Process.GoModule = processId
		} else {
			defs.Process.GoModule = name
		}
	}
	if defs.Process.GoPackage == "" {
		defs.Process.GoPackage = processId
	}
	if defs.Process.GoStartVariableName == "" {
		defs.Process.GoStartVariableName = "data"
	}
	if defs.Process.GoStartVariableType == "" {
		defs.Process.GoStartVariableType = "Data"
	}
}

// validateProcessMeta checks if the process has valid meta data specified
func validateProcessMeta(process Process) error {
	if process.GoPackage == "" {
		return errors.New("missing go package name in .bpmn")
	}
	return nil
}

// NewElement creates a new element of a given BPMN type and xml src object
func NewElement(elementType string, src any) (*Element, error) {
	idName, ok := src.(IDName)
	if !ok {
		return nil, fmt.Errorf("cannot convert element type '%s' to IDName for src\n", elementType)
	}
	element := Element{
		Id:            idName.GetId(),
		Name:          idName.GetName(),
		BpmnType:      elementType,
		XMLElement:    src,
		NextStateCode: "",
	}
	return &element, nil
}

// AddElementToProcess creates a new element based on the XML element, type, and id and adds it to
// the current process and lookup for all elements in the process
func AddElementToProcess(process *Process, xe any, elementType, elementId string) error {
	element, err := NewElement(elementType, xe)
	if err != nil {
		return err
	}
	process.Elements = append(process.Elements, element)
	process.elementMap[elementId] = element
	process.AllElements = append(process.AllElements, element)
	process.allElementMap[elementId] = element
	return nil
}

// AddElementToSubProcess creates a new element based on the XML element, type, and id and adds it to
// the current subprocess and lookup for all elements in the process
func AddElementToSubProcess(process *Process, subprocess *SubProcess, xe any, elementType, elementId string) error {
	element, err := NewElement(elementType, xe)
	if err != nil {
		return err
	}
	subprocess.Elements = append(subprocess.Elements, element)
	subprocess.elementMap[elementId] = element
	process.AllElements = append(process.AllElements, element)
	process.allElementMap[elementId] = element
	return nil
}

// warnMissingElement warns if an element is missing in the processing
func warnMissingElement(e string) {
	log.Fatalf("Missing element '%v' when setting incoming/outgoing elements", e)
}

// GetProcessOutgoingElements returns the outgoing elements for an element.
// If there are no outgoing elements it returns nil
func GetProcessOutgoingElements(element any, process *Process) []*Element {
	var xmlOutgoing Outgoing
	xmlOutgoing, ok := (element).(Outgoing)
	if !ok {
		return nil
	}
	return getOutgoingElements(xmlOutgoing, process)
}

// GetProcessElement looks up an element in a process by element id
func GetProcessElement(element any, process *Process) *Element {
	var el IDName
	el = element.(IDName)
	currentElement, ok := process.allElementMap[el.GetId()]
	if !ok {
		panic(fmt.Sprintf("missing element: %s", el.GetId()))
	}
	return currentElement
}

// getOutgoingElements looks up all elements in the process based on the xmlOutgoing elements
func getOutgoingElements(xmlOutgoing Outgoing, process *Process) []*Element {
	outgoingElements := make([]*Element, 0)
	for _, outgoing := range xmlOutgoing.GetOutgoing() {
		outEl, ok := process.allElementMap[outgoing]
		if !ok {
			warnMissingElement(outgoing)
		}
		outgoingElements = append(outgoingElements, outEl)
	}
	return outgoingElements
}

// GenerateCode generates the code for a process by sequentially calling the element processors in order.
// Order can matter in some cases such as subprocesses.
func GenerateCode(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, ep := range ps.ElementProcessors {
		ep.GenerateElementNextStateCodes(process, defs, ps)
	}
}

// generateCode generates code given the XMLBPMNDefinitions and a template and processor set
func generateCode(defs *XMLBPMNDefinitions, procTmpl string, ps *ProcessorSet) (*ProcessCode, error) {
	process, err := generateProcess(defs, ps)
	if err != nil {
		return nil, err
	}

	var temp *template.Template
	temp, err = template.New("Process").
		Funcs(template.FuncMap{"getUserTaskGoDataType": getUserTaskGoDataType}).
		Parse(procTmpl)
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, process)
	if err != nil {
		fmt.Println("error executing template generation: ")
		return nil, err
	}
	byteCode, err := format.Source([]byte(buf.String()))
	if err != nil {
		fmt.Printf("error formatting code:\n%s\n", buf.String())
		return nil, err
	}
	code := string(byteCode)
	return &ProcessCode{process, &code}, nil
}

// getUserTaskGoDataType gets the go data type for a user task optionally specified in the XML
// If not provided in the xml the default is dynamap.BaseTask
func getUserTaskGoDataType(element *Element) string {
	dataType := element.XMLElement.(*XMLUserTask).GoDataType
	if dataType == "" {
		dataType = "dynamap.BaseTask"
	}
	return dataType
}

// IsBPMN determines if an input is a bpmn file based on the extension
func IsBPMN(input string) bool {
	return strings.HasSuffix(input, ".bpmn")
}

// IsDir checks if an input is a directory
func IsDir(input string) bool {
	return input == "." || input == "/" || input == "\\"
}

// GetModuleName gets the name of a module given a fileName for the go.mod
func GetModuleName(fileName string) (string, error) {
	modData, err := os.ReadFile(fileName)
	if err != nil {
		return "", err
	}
	file, err := modfile.Parse(fileName, modData, nil)
	if err != nil {
		return "", err
	}
	if file.Module == nil {
		return "", fmt.Errorf("missing go.mod module name")
	}
	return file.Module.Mod.Path, nil
}
