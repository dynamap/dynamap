package builder

import "log"

type SubProcessProcessor struct {
}

func (s SubProcessProcessor) CreateElements(process *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.SubProcesses {
		xe := xe
		if subprocess == nil {
			// We are in the parent process, add elements as normal
			err := AddElementToProcess(process, &xe, "subProcess", xe.Id)
			if err != nil {
				return err
			}
			newSubProcess := SubProcess{
				Id:         xe.Id,
				Name:       xe.Name,
				Elements:   make([]*Element, 0),
				elementMap: make(map[string]*Element),
			}
			process.SubProcesses = append(process.SubProcesses, &newSubProcess)
			process.subProcessMap[newSubProcess.Id] = &newSubProcess
			CreateElements(process, &newSubProcess, &xe, &xe.XMLBPMNElements, ps)
		} else {
			// In subprocess, e.g. nested subprocess in subprocess
			err := AddElementToSubProcess(process, subprocess, &xe, "subProcess", xe.Id)
			if err != nil {
				return err
			}
			newSubProcess := SubProcess{
				Id:         xe.Id,
				Name:       xe.Name,
				Elements:   make([]*Element, 0),
				elementMap: make(map[string]*Element),
			}
			subprocess.SubProcesses = append(subprocess.SubProcesses, &newSubProcess)
			CreateElements(process, &newSubProcess, &xe, &xe.XMLBPMNElements, ps)
		}
	}
	return nil
}

func (s SubProcessProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.SubProcesses {
		GenerateCode(process, &e.XMLBPMNElements, ps)
		GenerateNextStateCode(&e, process, s, ps)
	}
}

func (s SubProcessProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return GenerateCodeForSingleOutgoingElement(currentElement, nextElements, process, ps)
}

func (s SubProcessProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	xmlsp, _ := newElement.XMLElement.(*XMLSubProcess)
	if len(xmlsp.StartEvents) != 1 {
		log.Fatalf("expected 1 start event in subProcess, got %d", len(xmlsp.StartEvents))
	}
	code := "id := g.GenerateId(dynamap.SubProcessType)\n"
	isLoop := xmlsp.MultiInstanceLoopCharacteristics.LoopCardinalityAtt != ""
	sp := process.subProcessMap[xmlsp.Id]
	if isLoop {
		code += "\t\tnumberOfInstances := " + xmlsp.MultiInstanceLoopCharacteristics.LoopCardinalityAtt + "\n"
		code += "\t\tspGetNextElement := func(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider, loopCounter int) dynamap.ElementStateProvider {\n"
		code += "\t\tswitch currentElement.GetElementState().Key {\n"
		for i := range sp.Elements {
			e := sp.Elements[i]
			code += "\t\tcase " + e.Id + ":\n"
			code += e.NextStateCode + "\n"
		}
		code += "\t\t}\n"
		code += "\t\t\treturn nil\n\t\t}\n"
		code += "\t\treturn dynamap.NewSubProcess(t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id, engine, g, " + xmlsp.StartEvents[0].Id + ", numberOfInstances, spGetNextElement)"
	} else {
		code += "\t\tspGetNextElement := func(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider, loopCounter int) dynamap.ElementStateProvider {\n"
		code += "\t\tswitch currentElement.GetElementState().Key {\n"
		for i := range sp.Elements {
			e := sp.Elements[i]
			code += "\t\tcase " + e.Id + ":\n"
			code += e.NextStateCode + "\n"
		}
		code += "\t\t}\n"
		code += "\t\t\treturn nil\n\t\t}\n"
		code += "\t\treturn dynamap.NewSubProcess(t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id, engine, g, " + xmlsp.StartEvents[0].Id + ", 1, spGetNextElement)"
	}
	return code
}
