package builder

// Todo: change references to getSequenceFlowCode to lookup processor sequence flows and invoke GenerateCodeForNewElement method

import (
	"errors"
	"fmt"
	"log"
)

type ExclusiveGatewayProcessor struct {
}

func (ep ExclusiveGatewayProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.ExclusiveGateways {
		xe := xe
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "exclusiveGateway", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "exclusiveGateway", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (ep ExclusiveGatewayProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.ExclusiveGateways {
		GenerateNextStateCode(&e, process, ep, ps)
	}
}

func (ep ExclusiveGatewayProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	if currentElement.BpmnType != "exclusiveGateway" {
		panic(currentElement.BpmnType)
	}
	return "gw := currentElement.(*dynamap.ExclusiveGateway)\n\t\treturn gw.NextElement"
}

func (ep ExclusiveGatewayProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	code :=
		`id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {`
	gwLogic, err := getExclusiveGatewayLogicCode(newElement, process)
	if err != nil {
		log.Fatalf("Could not get exclusive gateway logic code: " + err.Error())
	}
	code += gwLogic
	code += `
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ` + newElement.Id + `, id, gwHandler)`
	return code
}

func getExclusiveGatewayLogicCode(element *Element, process *Process) (string, error) {
	xmlElementId := element.XMLElement.(IDName).GetId()
	if xmlElementId != element.Id {
		panic(fmt.Sprintf("Got xml element id '%v', want '%v'", xmlElementId, element.Id))
	}
	gw := element.XMLElement.(*XMLExclusiveGateway)
	var defaultFlowElement *Element
	if gw.Default != "" {
		defaultFlowElement = process.allElementMap[gw.Default]
	}
	var xmlOutgoing Outgoing
	xmlOutgoing, ok := element.XMLElement.(Outgoing)
	if !ok {
		return "", errors.New("no outgoing elements on exclusive gateway")
	}
	strOutgoing := xmlOutgoing.GetOutgoing()
	if len(strOutgoing) < 1 {
		return "", errors.New("no outgoing elements on exclusive gateway")
	} else if len(strOutgoing) == 1 {
		outElement, ok := process.allElementMap[strOutgoing[0]]
		if !ok {
			return "", errors.New("could not look up outgoing element " + strOutgoing[0])
		}
		if outElement.BpmnType != "sequenceFlow" {
			return "", errors.New("expected sequence flow out of exclusive gateway")
		}
		return "\n\t\t\tid := g.GenerateId(dynamap.SequenceFlowType)\n" +
			"\t\t\treturn dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, " + outElement.Id + ", id)", nil
	} else {
		numOutgoing := len(strOutgoing)

		outElements := make([]*Element, numOutgoing, numOutgoing)
		for i, strOut := range strOutgoing {
			outElements[i] = process.allElementMap[strOut]
		}
		numElseIfFlow := numOutgoing - 1 // subtract first if condition
		if defaultFlowElement != nil {
			numElseIfFlow -= 1
		}

		// If statement
		firstIndex := 0
		if defaultFlowElement != nil && defaultFlowElement.Id == outElements[0].Id {
			firstIndex = 1
		}
		sf := outElements[firstIndex].XMLElement.(*XMLSequenceFlow)
		outExpression := sf.ConditionExpression
		code := "\t\t\tp.mu.RLock()\n\t\t\tdefer p.mu.RUnlock()"
		code += "\n\t\tif " + outExpression + " {\n\t\t" + getSequenceFlowCode(outElements[firstIndex]) + "\t\t}"

		// Else Ifs
		for i, outEl := range outElements {
			if i == firstIndex || (defaultFlowElement != nil && outEl.Id == defaultFlowElement.Id) {
				continue // Ignore first if and default
			}
			sf := outEl.XMLElement.(*XMLSequenceFlow)
			outExpression := sf.ConditionExpression
			code += " else if " + outExpression + "{\n\t\t" + getSequenceFlowCode(outEl) + "\t\t}"
		}

		// Else
		if defaultFlowElement != nil {
			code += " else {" + getSequenceFlowCode(defaultFlowElement) + "\t\t}"
		} else {
			// No Default Flow
			code += "\npanic(\"No Default Flow!\")\n"
			code += "\nreturn nil"
		}
		return code, nil
	}
}
