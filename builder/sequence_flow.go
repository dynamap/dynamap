package builder

// Todo: delete getSequenceFlowCode after dependencies removed in exclusive_gateway

type SequenceFlowProcessor struct {
}

func (sf SequenceFlowProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.SequenceFlows {
		xe := xe
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "sequenceFlow", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "sequenceFlow", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (sf SequenceFlowProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.SequenceFlows {
		GenerateNextStateCode(&e, process, sf, ps)
	}
}

func (sf SequenceFlowProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return GenerateCodeForSingleOutgoingElement(currentElement, nextElements, process, ps)
}

func (sf SequenceFlowProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	return "id := g.GenerateId(dynamap.SequenceFlowType)\n" +
		"\t\treturn dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id)"
}

func getSequenceFlowCode(element *Element) string {
	return "id := g.GenerateId(dynamap.SequenceFlowType)\n" +
		"\t\treturn dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, " + element.Id + ", id)"
}
