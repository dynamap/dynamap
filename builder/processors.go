package builder

const (
	subProcess       = "subProcess"       // key for subprocess processors
	sequenceFlow     = "sequenceFlow"     //  key for sequenceFlow processors
	startEvent       = "startEvent"       // key for startEvent processors
	endEvent         = "endEvent"         // key for endEvent processors
	exclusiveGateway = "exclusiveGateway" // key for exclusiveGateway processors
	parallelGateway  = "parallelGateway"  // key for parallelGateway processors
	userTask         = "userTask"         // key for userTask processors
	scriptTask       = "scriptTask"       // key for scriptTask processors
)

// ElementProcessor generates code for a specific BPMN element type
type ElementProcessor interface {
	// CreateElements creates new elements and adds them to the process and/or subprocess for the given element BPMN processor type
	CreateElements(process *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error
	// GenerateElementNextStateCodes generates the code for the next state for each element of the element BPMN processor type
	GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet)
	// GenerateNextStateCode generates the code of the next elements for the current element
	GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string
	// GenerateCodeForNewElement generates code for a new instance of this BPMN element
	GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string
}

// ProcessorSet contains a set of processors
type ProcessorSet struct {
	ElementProcessors   []ElementProcessor          // Ordered sequence of element processors
	ElementProcessorMap map[string]ElementProcessor // Look up processors by key
}

// AddElementProcessor adds an element processor to be executed by the code generator in the order they are added. Order may matter for some
// processors such as subprocesses.
func (p *ProcessorSet) AddElementProcessor(elementType string, e ElementProcessor) {
	p.ElementProcessors = append(
		p.ElementProcessors,
		e,
	)
	p.ElementProcessorMap[elementType] = e
}

// SetElementProcessor sets the processor for a specific element type. If a processor already exists,
// it is replaced with the processor passed in
func (p *ProcessorSet) SetElementProcessor(elementType string, e ElementProcessor) {
	existing, ok := p.ElementProcessorMap[elementType]
	if !ok {
		p.AddElementProcessor(elementType, e)
		return
	}
	for i := range p.ElementProcessors {
		if p.ElementProcessors[i] == existing {
			p.ElementProcessors[i] = e
			p.ElementProcessorMap[elementType] = e
			return
		}
	}
}

// NewProcessorSet creates an empty processor set. A custom set of element processors may be created by calling
// AddElementProcessor
func NewProcessorSet() *ProcessorSet {
	ps := ProcessorSet{}
	ps.ElementProcessors = make([]ElementProcessor, 0)
	ps.ElementProcessorMap = make(map[string]ElementProcessor, 0)
	return &ps
}

// NewDefaultProcessorSet creates the default processor set
func NewDefaultProcessorSet() *ProcessorSet {
	ps := NewProcessorSet()
	ps.AddElementProcessor(subProcess, SubProcessProcessor{}) // SubProcessProcessor must execute first so child elements are processed.
	ps.AddElementProcessor(sequenceFlow, SequenceFlowProcessor{})
	ps.AddElementProcessor(startEvent, StartEventProcessor{})
	ps.AddElementProcessor(endEvent, EndEventProcessor{})
	ps.AddElementProcessor(exclusiveGateway, ExclusiveGatewayProcessor{})
	ps.AddElementProcessor(parallelGateway, ParallelGatewayProcessor{})
	ps.AddElementProcessor(userTask, UserTaskProcessor{})
	ps.AddElementProcessor(scriptTask, ScriptProcessor{})
	return ps
}

// GenerateCodeForSingleOutgoingElement generates the code for the current element assuming there is only one outgoing element.
func GenerateCodeForSingleOutgoingElement(currentElement *Element, outgoingElements []*Element, process *Process, ps *ProcessorSet) string {
	outgoingElement := outgoingElements[0]
	processor := ps.ElementProcessorMap[outgoingElement.BpmnType]
	return processor.GenerateCodeForNewElement(currentElement, outgoingElement, process, ps)
}

// GenerateNextStateCode generates the code for the next state that comes after the current BPMN element
func GenerateNextStateCode(element any, process *Process, processor ElementProcessor, ps *ProcessorSet) {
	currentElement := GetProcessElement(element, process)
	outgoingElements := GetProcessOutgoingElements(element, process)
	if outgoingElements == nil {
		currentElement.NextStateCode = "return nil"
		return
	}
	currentElement.NextStateCode = processor.GenerateNextStateCode(currentElement, outgoingElements, process, ps)
}
