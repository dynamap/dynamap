package builder

type UserTaskProcessor struct {
}

func (up UserTaskProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.UserTasks {
		xe := xe
		if subprocess == nil {
			element, err := NewElement("userTask", &xe)
			if err != nil {
				return err
			}
			p.Elements = append(p.Elements, element)
			p.elementMap[xe.Id] = element
			p.UserTasks = append(p.UserTasks, element)
			p.AllElements = append(p.AllElements, element)
			p.allElementMap[xe.Id] = element
		} else {
			element, err := NewElement("userTask", &xe)
			if err != nil {
				return err
			}
			subprocess.Elements = append(subprocess.Elements, element)
			subprocess.elementMap[xe.Id] = element
			p.UserTasks = append(p.UserTasks, element)
			p.AllElements = append(p.AllElements, element)
			p.allElementMap[xe.Id] = element
		}
	}
	return nil
}

func (up UserTaskProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.UserTasks {
		GenerateNextStateCode(&e, process, up, ps)
	}
}

func (up UserTaskProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return GenerateCodeForSingleOutgoingElement(currentElement, nextElements, process, ps)
}

func (up UserTaskProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	taskType := "dynamap.BaseTask"
	t := newElement.XMLElement.(*XMLUserTask)
	if t.GoDataType != "" {
		taskType = t.GoDataType
	}

	return "id := g.GenerateId(dynamap.UserTaskType)\n" +
		"\t\treturn dynamap.NewUserTask[" + taskType + "](t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id, \"\", true)"
}
