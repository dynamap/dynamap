package builder

type EndEventProcessor struct {
}

func (ep EndEventProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for _, xe := range elements.EndEvents {
		xe := xe
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "endEvent", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "endEvent", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (ep EndEventProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.EndEvents {
		GenerateNextStateCode(&e, process, ep, ps)
	}
}

func (ep EndEventProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return "return nil"
}

func (ep EndEventProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	return "id := g.GenerateId(dynamap.NoneEndEventType)\n" +
		"\t\treturn dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, " + newElement.Id + ", id, true)"
}
