package main

import (
	_ "embed"
	"flag"
	"fmt"
	"gitlab.com/dynamap/dynamap/builder"
	"log"
	"os"
	"path/filepath"
)

var input = flag.String("input", ".", "input bpmn file or directory containing bpmn files")
var output = flag.String("output", ".", "output directory for code")
var processTemplateFile = flag.String("proctmplfile", "", "process template file to use for generated code")
var packageName = flag.String("pkg", "", "override default package name")
var ver = flag.String("ver", "1.0.0", "dumb version")

// embed this template to avoid distributing with the adjoining process.got file
// i.e. standalone executable
//go:embed "process.got"
var gotProcess string

func main() {
	flag.Parse()
	genBPMN()
}

func determineOutputName(cdir, name string) string {
	if name[0] == '/' {
		// assume explicit full path and just use verbatim
		return name
	}
	return filepath.Join(cdir, name)
}

func genBPMN() {
	if !builder.IsBPMN(*input) && !builder.IsDir(*input) {
		fmt.Printf("input '%s' must be a directory or file ending with .bpmn\n", *input)
		os.Exit(-1)
	}
	cdir, err := os.Getwd()
	if err != nil {
		fmt.Printf("error getting current directory: %v\n", err)
		os.Exit(-1)
	}
	*input = filepath.Join(cdir, *input)
	outDir := determineOutputName(cdir, *output)
	fmt.Printf("Processing input: '%s'\n", *input)
	pg := builder.NewProcessGenerator(*input, outDir)

	if len(*packageName) != 0 {
		pg.PackageName = *packageName
	}

	if *processTemplateFile == "" {
		pg.ProcessTemplate = gotProcess
	} else {
		t, err := os.ReadFile(*processTemplateFile)
		if err != nil {
			log.Printf("ReadFile error %v", err)
			os.Exit(-1)
		}
		pg.ProcessTemplate = string(t)
	}

	pg.ProcessorSet = builder.NewDefaultProcessorSet()
	err = pg.GenerateAndWriteCodeFromBPMN()
	if err != nil {
		fmt.Printf("error: '%v'\n", err)
		return
	}
}
