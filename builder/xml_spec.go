package builder

// Todo: refactor to remove getUserTaskGoDataType from bpmn_generator, which shouldn't know anything about specific elements ideally. It should just invoke ElementProcessors

import (
	_ "embed"
	"encoding/xml"
	"io"
	"os"

	"gitlab.com/dynamap/dynamap"

)

type IDName interface {
	GetId() string
	GetName() string
}

type XMLIDName struct {
	Id   string `xml:"id,attr"`
	Name string `xml:"name,attr"`
}

func (x *XMLIDName) GetId() string {
	return x.Id
}

func (x *XMLIDName) GetName() string {
	return x.Name
}

type XMLIncomingElements struct {
	Incoming []string `xml:"incoming"`
}

type Incoming interface {
	GetIncoming() []string
}

type Outgoing interface {
	GetOutgoing() []string
}

type XMLOutgoingElements struct {
	Outgoing []string `xml:"outgoing"`
}

func (e *XMLIncomingElements) GetIncoming() []string {
	return e.Incoming
}

func (e *XMLOutgoingElements) GetOutgoing() []string {
	return e.Outgoing
}

type XMLTimerEventDefinition struct {
	XMLName xml.Name `xml:"timerEventDefinition"`
	XMLTimeCycle
}

type XMLTimeCycle struct {
	XMLName       xml.Name `xml:"timeCycle"`
	Type          string   `xml:"type,attr"`
	TimeCycle     string   `xml:"timeCycle"`
	ISO8601       dynamap.ISO8601DateTime
	DurationMicro int64
}

type XMLConditionExpression struct {
	XMLName             xml.Name `xml:"conditionExpression"`
	Type                string   `xml:"type,attr"`
	ConditionExpression string   `xml:"conditionExpression"`
}

type XMLBPMNDefinitions struct {
	XMLName xml.Name       `xml:"definitions"`
	Process XMLBPMNProcess `xml:"process"`
}

type Script interface {
	GetScript() string
}

type XMLScript struct {
	Script string `xml:"script"`
}

func (s *XMLScript) GetScript() string {
	return s.Script
}

type XMLBPMNElements struct {
	StartEvents       []XMLBPMNStartEvent   `xml:"startEvent"`
	ExclusiveGateways []XMLExclusiveGateway `xml:"exclusiveGateway"`
	ParallelGateways  []XMLParallelGateway  `xml:"parallelGateway"`
	SequenceFlows     []XMLSequenceFlow     `xml:"sequenceFlow"`
	UserTasks         []XMLUserTask         `xml:"userTask"`
	EndEvents         []XMLEndEvent         `xml:"endEvent"`
	ScriptTasks       []XMLScriptTask       `xml:"scriptTask"`
	SubProcesses      []XMLSubProcess       `xml:"subProcess"`
}

type XMLBPMNProcess struct {
	XMLIDName
	XMLBPMNElements
	XMLName             xml.Name `xml:"process"`
	GoModule            string   `xml:"module,attr"`
	GoPackage           string   `xml:"package,attr"`
	GoStartVariableName string   `xml:"startVariableName,attr"`
	GoStartVariableType string   `xml:"startVariableType,attr"`
}

type XMLBPMNStartEvent struct {
	XMLName xml.Name `xml:"startEvent"`
	XMLIDName
	XMLOutgoingElements
	TimerEventDefinition XMLTimerEventDefinition `xml:"timerEventDefinition"`
}

type XMLExclusiveGateway struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName xml.Name `xml:"exclusiveGateway"`
	Default string   `xml:"default,attr"`
}

type XMLParallelGateway struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName xml.Name `xml:"parallelGateway"`
}

type XMLSequenceFlow struct {
	XMLIDName
	XMLConditionExpression
	XMLName   xml.Name `xml:"sequenceFlow"`
	SourceRef string   `xml:"sourceRef,attr"`
	TargetRef string   `xml:"targetRef,attr"`
}

func (x *XMLSequenceFlow) GetOutgoing() []string {
	return []string{x.TargetRef}
}

type XMLUserTask struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName    xml.Name `xml:"userTask"`
	GoDataType string   `xml:"dataType,attr"`
}

type XMLEndEvent struct {
	XMLIDName
	XMLIncomingElements
	XMLName xml.Name `xml:"endEvent"`
}

type XMLScriptTask struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLScript
	XMLName xml.Name `xml:"scriptTask"`
}

type XMLMultiInstanceLoopCharacteristics struct {
	IsSequentialAtt    string `xml:"isSequential,attr"`
	LoopCardinalityAtt string `xml:"loopCardinality"`
}

type XMLSubProcess struct {
	XMLIDName
	XMLBPMNElements
	XMLIncomingElements
	XMLOutgoingElements
	XMLName                          xml.Name                            `xml:"subProcess"`
	MultiInstanceLoopCharacteristics XMLMultiInstanceLoopCharacteristics `xml:"multiInstanceLoopCharacteristics"`
}

func parseBPMNModelFromFile(fileName string) (*XMLBPMNDefinitions, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return parseBPMNModel(file)
}

func parseBPMNModel(r io.Reader) (*XMLBPMNDefinitions, error) {
	var defs XMLBPMNDefinitions
	err := xml.NewDecoder(r).Decode(&defs)
	return &defs, err
}
