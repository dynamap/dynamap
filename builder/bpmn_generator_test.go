package builder

import (
	_ "embed"
	"testing"
)

//go:embed "cmd/dynamap/process.got"
var gotProcess string

func check(t *testing.T, got, want string) {
	if got != want {
		t.Errorf("Got '%s', want '%s'", got, want)
	}
}

func TestGenerateFromBPMNFileProcess(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	check(t, p.Id, "StartPlanProcess")
	check(t, p.Name, "Plan Process")
	check(t, p.GoPackage, "plan")
	check(t, p.GoStartVariableName, "plan")
	check(t, p.GoStartVariableType, "Plan")
}

func TestGenerateFromBPMNFileStartEvent(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	if len(p.StartEvents) == 0 {
		t.Fatalf("Got 0 start events, expected 1")
	}
	check(t, p.StartEvents[0].Id, "StartEvent_1")
	check(t, p.StartEvents[0].Name, "Start Plan")
	check(t, p.StartEvents[0].Outgoing[0], "SequenceFlow_1rgyt8e")
}

func TestGenerateFromBPMNFileTimerStartEvent(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/timer/timer.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	if len(p.StartEvents) == 0 {
		t.Fatalf("Got 0 start events, expected 1")
	}
	check(t, p.StartEvents[0].Id, "Start")
	check(t, p.StartEvents[0].Name, "Every 1 Second")
	check(t, p.StartEvents[0].Outgoing[0], "SequenceFlow_0ci9n6x")
	check(t, p.StartEvents[0].TimerEventDefinition.TimeCycle, "R/PT1S")
}

func TestGenerateFromBPMNFileEndEvent(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	if len(p.EndEvents) == 0 {
		t.Fatalf("Got 0 end events, expected 1")
	}
	check(t, p.EndEvents[0].Id, "PlanningEndEvent")
	check(t, p.EndEvents[0].Name, "End Planning")
	check(t, p.EndEvents[0].Incoming[0], "SequenceFlow_0ibwzxj")
}

func TestGenerateFromBPMNFileExclusiveGateways(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	if len(p.ExclusiveGateways) != 2 {
		t.Fatalf("Got %d end events, expected 2", len(p.ExclusiveGateways))
	}
	check(t, p.ExclusiveGateways[0].Id, "ExclusiveGateway_1clbvj4")
	check(t, p.ExclusiveGateways[0].Name, "Fusion Required?")
	check(t, p.ExclusiveGateways[0].Default, "SequenceFlow_0hbc8s4")
	check(t, p.ExclusiveGateways[1].Id, "ExclusiveGateway_0k2bk5r")
	check(t, p.ExclusiveGateways[1].Name, "")
	check(t, p.ExclusiveGateways[1].Default, "")
}

func TestGenerateFromBPMNFileUserTask(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Errorf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	if len(p.UserTasks) != 3 {
		t.Errorf("Got %d user tasks, expected 3", len(p.UserTasks))
	}
	check(t, p.UserTasks[0].Id, "FusionTask")
	check(t, p.UserTasks[0].Name, "Fusion")
	check(t, p.UserTasks[0].Incoming[0], "SequenceFlow_0hbc8s4")
	check(t, p.UserTasks[0].Outgoing[0], "SequenceFlow_04cepq3")

	check(t, p.UserTasks[1].Id, "ContouringTask")
	check(t, p.UserTasks[1].Name, "Contouring")
	check(t, p.UserTasks[1].Incoming[0], "SequenceFlow_1fdvgyu")
	check(t, p.UserTasks[1].Outgoing[0], "SequenceFlow_0dpgu80")
}

func TestGenerateFromBPMNFileSequenceFlows(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	p := defs.Process
	got := len(p.SequenceFlows)
	want := 11
	if got != want {
		t.Fatalf("Got %d sequence flows, expected %d", len(p.SequenceFlows), want)
	}
	checkSequenceFlow := func(t *testing.T, flow XMLSequenceFlow, id, sourceRef, targetRef, condition string) {
		if flow.Id != id {
			t.Errorf("Got id '%s', want '%s'", flow.Id, id)
		}
		if flow.SourceRef != sourceRef {
			t.Errorf("Got sourceRef '%s', want '%s'", flow.SourceRef, sourceRef)
		}
		if flow.TargetRef != targetRef {
			t.Errorf("Got targetRef '%s', want '%s'", flow.TargetRef, targetRef)
		}
		if flow.ConditionExpression != condition {
			t.Errorf("Got conditionExpression '%s', want '%s'", flow.ConditionExpression, condition)
		}
	}
	checkSequenceFlow(t, p.SequenceFlows[0], "SequenceFlow_1rgyt8e", "StartEvent_1", "ExclusiveGateway_1clbvj4", "")
	checkSequenceFlow(t, p.SequenceFlows[1], "SequenceFlow_0hbc8s4", "ExclusiveGateway_1clbvj4", "FusionTask", "")
	checkSequenceFlow(t, p.SequenceFlows[2], "SequenceFlow_04cepq3", "FusionTask", "ExclusiveGateway_0k2bk5r", "")
	checkSequenceFlow(t, p.SequenceFlows[3], "SequenceFlow_0tnr42a", "ExclusiveGateway_1clbvj4", "ExclusiveGateway_0k2bk5r", "!p.plan.Fusion")
	checkSequenceFlow(t, p.SequenceFlows[4], "SequenceFlow_0zwsz2i", "ExclusiveGateway_0k2bk5r", "ParallelGateway_09iq80z", "")
	checkSequenceFlow(t, p.SequenceFlows[7], "SequenceFlow_0dpgu80", "ContouringTask", "ParallelGateway_06j37h3", "")
}

func TestGenerateCodePlan(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/plan/plan.bpmn")
	if err != nil {
		t.Fatalf("Got error parsing model %v, expected nil error", err)
	}
	ps := NewDefaultProcessorSet()
	_, err = generateCode(defs, gotProcess, ps)
	if err != nil {
		t.Fatalf("Got error generating code %v, expected nil error", err)
	}
}

func TestGenerateCodeRisk(t *testing.T) {
	defs, err := parseBPMNModelFromFile("../internal/risk/risk.bpmn")
	if err != nil {
		t.Fatalf("Got error %v, expected nil error", err)
	}
	ps := NewDefaultProcessorSet()
	code, err := generateCode(defs, gotProcess, ps)
	if err != nil {
		t.Fatalf("Code:\n%v\nGot error %v, expected nil error", code, err)
	}
}
