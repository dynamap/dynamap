package builder

import "testing"

func TestSetElementProcessorOverride(t *testing.T) {
	ps := NewDefaultProcessorSet()
	initLen := len(ps.ElementProcessors)

	// Override processor for sequence flow
	e := ExclusiveGatewayProcessor{}
	ps.SetElementProcessor(sequenceFlow, e)

	gotE := ps.ElementProcessorMap[sequenceFlow]
	if gotE != e {
		t.Fatalf("got element processor %p, want  %p", gotE, e)
	}

	afterLen := len(ps.ElementProcessors)
	if initLen != afterLen {
		t.Fatalf("got %d processors, expected %d", afterLen, initLen)
	}
}
