package builder

import (
	"gitlab.com/dynamap/dynamap"
	"log"
)

type StartEventProcessor struct {
}

func (sp StartEventProcessor) CreateElements(p *Process, subprocess *SubProcess, spElements *XMLSubProcess, elements *XMLBPMNElements, ps *ProcessorSet) error {
	for i, xe := range elements.StartEvents {
		xe := xe
		if xe.TimerEventDefinition.TimeCycle != "" {
			d, err := dynamap.NewISO8601DateTime(xe.TimerEventDefinition.TimeCycle)
			if err != nil {
				log.Printf("NewISO8601DateTime warning: %+v", err)
				//				return err
			}
			// if the time cycle containted quotes the d.Value field should be clean of those
			xe.TimerEventDefinition.TimeCycle = d.Value
			dur, err := d.Duration.Duration()
			if err != nil {
				log.Printf("warning: %+v %v", err, dur)
				//				return err
			}
			durInt := int64(dur.Microseconds())
			xe.TimerEventDefinition.ISO8601 = d
			xe.TimerEventDefinition.DurationMicro = durInt
			elements.StartEvents[i].TimerEventDefinition.ISO8601 = d
			elements.StartEvents[i].TimerEventDefinition.DurationMicro = durInt

		}
		if subprocess == nil {
			err := AddElementToProcess(p, &xe, "startEvent", xe.Id)
			if err != nil {
				return err
			}
		} else {
			err := AddElementToSubProcess(p, subprocess, &xe, "startEvent", xe.Id)
			if err != nil {
				return err
			}
		}
	}
	if len(elements.StartEvents) > 0 {
		p.StartEvent = p.elementMap[elements.StartEvents[0].Id]
	}
	return nil
}

func (sp StartEventProcessor) GenerateElementNextStateCodes(process *Process, defs *XMLBPMNElements, ps *ProcessorSet) {
	for _, e := range defs.StartEvents {
		GenerateNextStateCode(&e, process, sp, ps)
	}
}

func (sp StartEventProcessor) GenerateNextStateCode(currentElement *Element, nextElements []*Element, process *Process, ps *ProcessorSet) string {
	return GenerateCodeForSingleOutgoingElement(currentElement, nextElements, process, ps)
}

func (sp StartEventProcessor) GenerateCodeForNewElement(currentElement, newElement *Element, process *Process, ps *ProcessorSet) string {
	return ""
}
