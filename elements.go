package dynamap

import (
	"context"
)

const (
	ElementCreating    = "Creating"
	ElementCreated     = "Created"
	ElementActivating  = "Activating"
	ElementActivated   = "Activated"
	ElementCompleting  = "Completing"
	ElementCompleted   = "Completed"
	ElementTerminating = "Terminating"
	ElementTerminated  = "Terminated"
)

const (
	ProcessInstanceType            = "ProcessInstance"
	TokenType                      = "Token"
	SequenceFlowType               = "SequenceFlow"
	ExclusiveGatewayType           = "ExclusiveGateway"
	ParallelGatewayType            = "ParallelGateway"
	NoneStartEventType             = "NoneStartEvent"
	NoneIntermediateThrowEventType = "NoneIntermediateThrowEvent"
	NoneEndEventType               = "NoneEndEvent"
	TimerStartEventType            = "TimerStartEventType"
	UserTaskType                   = "UserTaskType"
	ScriptTaskType                 = "ScriptTaskType"
	SubProcessType                 = "SubProcess"
)

var (
	ErrDataUnmarshal = NewBPMNEngineError(nil, "")
)

// LifeCycleRunner runs the life cycle of an element through all required states for the element such as Creating to Completed.
type LifeCycleRunner interface {
	RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error
}

// ElementState contains the primary information required to describe the state of any element.
type ElementState struct {
	ElementType       string // The type of element, e.g. SequenceFlow, NoneStartEvent, ParallelGateway, etc.
	Key               string // The unique name for an element in a process at design time.
	Id                string // The unique id created at run-time for an instance of the element.
	Status            string // The status of the element state, e.g. Completing, Completed, etc.
	ProcessInstanceId string // The id of the process instance it belongs to
	TokenId           string // The id of the token it belongs to
	Object            any    `json:"-"` // A reference to the underlying element if required for direct access, e.g. to enable custom storage.
	Store             bool   // Whether to store the element state
}

// ElementStateProvider is an interface that provides the state of an element.
type ElementStateProvider interface {
	GetElementState() ElementState
}
