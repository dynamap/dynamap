package dynamap

import (
	"context"
)

type SequenceFlow struct {
	ElementState
}

func (s *SequenceFlow) GetElementState() ElementState {
	return s.ElementState
}

func NewSequenceFlow(pid, tid, key, id string) *SequenceFlow {
	return &SequenceFlow{
		ElementState{
			ProcessInstanceId: pid,
			TokenId:           tid,
			Key:               key,
			Id:                id,
			ElementType:       SequenceFlowType,
		},
	}
}

func (s *SequenceFlow) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	s.Status = ElementCompleted
	if s.Store {
		bpmnEngine.WriteElement(s.ElementState)
	}
	return nil
}
