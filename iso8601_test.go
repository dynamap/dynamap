package dynamap

import (
	"testing"
	"time"
)

func TestISO8601Repeating(t *testing.T) {
	value := "R"
	dt, _ := NewISO8601DateTime(value)
	if !(dt.IsRepeating && !dt.IsDuration && !dt.IsIndefinitely) {
		t.Errorf("Expected repeating set for '%s', got %+v", value, dt)
	}
}

func TestISO8601RepeatingIndefinitely(t *testing.T) {
	value := "R/"
	dt, _ := NewISO8601DateTime(value)
	if !(dt.IsRepeating && dt.IsIndefinitely && !dt.IsDuration) {
		t.Errorf("Expected repeating indefinitely set for '%s', got %+v", value, dt)
	}
}

func TestISO8601RepeatingIndefinitelyDuration(t *testing.T) {
	value := "R/P"
	dt, _ := NewISO8601DateTime(value)
	if !(dt.IsDuration && dt.IsRepeating && dt.IsIndefinitely) {
		t.Errorf("Expected duration repeating indefinitely set for '%s', got %+v", value, dt)
	}
}

func TestNonDuration(t *testing.T) {
	values := []string{"R/a", "R/0", "A", "0"}
	for _, v := range values {
		dt, err := NewISO8601DateTime(v)
		if dt.IsDuration {
			t.Errorf("Got duration for value '%s', expected non-duration", v)
		}
		if err == nil {
			t.Errorf("Expected error for non-duration value '%s'", v)
		}
	}
}

func TestConvDurations(t *testing.T) {
	type DurationTest struct {
		value string
		want  ISO8601DateTime
	}
	tests := []DurationTest{
		{"P1Y", ISO8601DateTime{
			Value:          "P1Y",
			Duration:       Duration{Year: "1"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"P5M", ISO8601DateTime{
			Value:          "P5M",
			Duration:       Duration{Month: "5"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"P3D", ISO8601DateTime{
			Value:          "P3D",
			Duration:       Duration{Day: "3"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"P2Y3M4D", ISO8601DateTime{
			Value:          "P2Y3M4D",
			Duration:       Duration{Year: "2", Month: "3", Day: "4"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"PT20H", ISO8601DateTime{
			Value:          "PT20H",
			Duration:       Duration{Hour: "20"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"PT5M", ISO8601DateTime{
			Value:          "PT5M",
			Duration:       Duration{Minute: "5"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"PT20S", ISO8601DateTime{
			Value:          "PT20S",
			Duration:       Duration{Second: "20"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"PT10H9M8S", ISO8601DateTime{
			Value:          "PT10H9M8S",
			Duration:       Duration{Hour: "10", Minute: "9", Second: "8"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
		{"P0Y1M2DT10H9M8S", ISO8601DateTime{
			Value:          "P0Y1M2DT10H9M8S",
			Duration:       Duration{Year: "0", Month: "1", Day: "2", Hour: "10", Minute: "9", Second: "8"},
			IsDuration:     true,
			IsRepeating:    false,
			IsIndefinitely: false,
		}},
	}
	for _, test := range tests {
		got, err := NewISO8601DateTime(test.value)
		if err != nil {
			t.Errorf("unexpected error '%s' for value '%s'", err, test.value)
		}
		if got != test.want {
			t.Errorf("got date '%+v'\n want date '%+v'", got, test.want)
		}
	}
}

func TestGoDuration(t *testing.T) {
	type DurationTest struct {
		value string
		want  time.Duration
	}
	tests := []DurationTest{
		{"PT2S", 2 * time.Second},
		{"PT3M", 3 * time.Minute},
		{"PT4H", 4 * time.Hour},
		{"PT5D", 5 * 24 * time.Hour},
		{"P6M", 6 * 365.0 / 12 * 24 * time.Hour},
		{"P2Y", 2 * 365 * 24 * time.Hour},
		{"P1Y1M1DT1H1M1S", 9515*time.Hour + 1*time.Minute + 1*time.Second},
	}
	for _, test := range tests {
		v, _ := NewISO8601DateTime(test.value)
		got, err := v.Duration.Duration()
		if err != nil {
			t.Errorf("unexpected error parsing value '%s': %s", test.value, err)
		}
		want := test.want
		if got != want {
			t.Errorf("got duration '%s', want '%s'", got, want)
		}
	}
}
