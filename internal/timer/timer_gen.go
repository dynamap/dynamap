package timer

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	SequenceFlow_0ci9n6x = "SequenceFlow_0ci9n6x"

	SequenceFlow_0gawtsr = "SequenceFlow_0gawtsr"

	SequenceFlow_1k9rpkj = "SequenceFlow_1k9rpkj"

	SequenceFlow_0eltyfl = "SequenceFlow_0eltyfl"

	SequenceFlow_1s44iah = "SequenceFlow_1s44iah"

	SequenceFlow_1t71n6w = "SequenceFlow_1t71n6w"

	Start = "Start"

	EndEvent = "EndEvent"

	ExclusiveGateway_08mjjnp = "ExclusiveGateway_08mjjnp"

	ExclusiveGateway_0en7ysa = "ExclusiveGateway_0en7ysa"

	RunCheckTask = "RunCheckTask"

	Task_1p0v7wv = "Task_1p0v7wv"
)

type TimerProcess struct {
	mu   sync.RWMutex
	data *Data

	// callback block
	start func()
	done  func()
}

func NewTimerProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data *Data) *dynamap.ProcessInstance {
	impl := &TimerProcess{
		//			mu: sync.RWMutex{},
		data:  data,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "TimerProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *TimerProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.data
}

type TimerEventStarterOption func(*TimerEventStarterConfig)

type TimerEventStarterConfig struct {
	newData func() *Data
}

func NewTimerEventStarterConfig() *TimerEventStarterConfig {
	return &TimerEventStarterConfig{
		newData: func() *Data { return &Data{} },
	}
}

func WithTimerEventStarterData(f func() *Data) TimerEventStarterOption {
	return func(cfg *TimerEventStarterConfig) {
		cfg.newData = f
	}
}

type TimerEventStarter struct {
	cfg        *TimerEventStarterConfig
	timerCycle time.Duration
}

func (p *TimerEventStarter) TimerSpec() string {
	return `R/PT1S`
}

func (tes *TimerEventStarter) ParseAndValidateCycleSpec(spec string) error {
	d, err := dynamap.NewISO8601DateTime(spec)
	if err != nil {
		return err
	}
	dur, err := d.Duration.Duration()
	if err != nil {
		return err
	}
	tes.timerCycle = dur
	return nil
}

func (p *TimerEventStarter) TimeAfter() <-chan time.Time {
	return time.After(1000000 * time.Microsecond)
}

func (tes *TimerEventStarter) TickAction(ctx context.Context, b *dynamap.BPMNEngine) {
	pid := b.IdGenerator.GenerateId(dynamap.ProcessInstanceType)
	startEvent := dynamap.NewTimerStartEvent(pid, "", "Start", b.IdGenerator.GenerateId(dynamap.TimerStartEventType))
	//	timerProcess := NewTimerProcess(pid, b, startEvent, &Data{})
	timerProcess := NewTimerProcess(pid, b, startEvent, tes.cfg.newData())
	b.SendCommand(dynamap.StartProcessInstanceCmd{Instance: timerProcess})
}

// This must implement the StartEventProcessor interface from engine.go
func (tes *TimerEventStarter) StartEventProcesses(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, opts ...any) error {
	tes.cfg = NewTimerEventStarterConfig()
	for _, opt := range opts {
		switch o := opt.(type) {
		case TimerEventStarterOption:
			o(tes.cfg)
		}
	}
	err := tes.ParseAndValidateCycleSpec(tes.TimerSpec())
	if err != nil {
		return err
	}
	go func() {
		// fire once at startup to avoid delays for longer cycles
		tes.TickAction(ctx, bpmnEngine)
		for {
			select {
			case <-ctx.Done():
				return
			case <-tes.TimeAfter():
				tes.TickAction(ctx, bpmnEngine)

			}
		}
	}()
	return nil
}

func (tes *TimerEventStarter) StartEventProcessesWithClock(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, clk dynamap.ClockSource, opts ...any) error {

	tes.cfg = NewTimerEventStarterConfig()
	for _, opt := range opts {
		switch o := opt.(type) {
		case TimerEventStarterOption:
			o(tes.cfg)
		}
	}

	tickChan := make(chan time.Time, 1)

	err := clk.SpecHandler(tes.TimerSpec(), func() {
		tickChan <- time.Now()
	})
	if err != nil {
		return err
	}
	go func() {
		for {
			select {
			case <-ctx.Done():
				clk.Stop()
				close(tickChan)
				return
			case <-tickChan:
				tes.TickAction(ctx, bpmnEngine)

			}
		}
	}()
	return nil
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *TimerProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case SequenceFlow_0ci9n6x:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0ci9n6x}
		outSeqKeys := []string{SequenceFlow_0gawtsr, SequenceFlow_1k9rpkj}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0en7ysa, id, SequenceFlow_0ci9n6x, inSeqKeys, outSeqKeys)

	case SequenceFlow_0gawtsr:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count++
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, Task_1p0v7wv, id, script)

	case SequenceFlow_1k9rpkj:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			time.Sleep(10 * time.Millisecond)
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, RunCheckTask, id, script)

	case SequenceFlow_0eltyfl:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_08mjjnp, id, SequenceFlow_0eltyfl, inSeqKeys, outSeqKeys)

	case SequenceFlow_1s44iah:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, EndEvent, id, true)

	case SequenceFlow_1t71n6w:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_08mjjnp, id, SequenceFlow_1t71n6w, inSeqKeys, outSeqKeys)

	case Start:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0ci9n6x, id)

	case EndEvent:
		return nil

	case ExclusiveGateway_08mjjnp:
		return nil

	case ExclusiveGateway_0en7ysa:
		return nil

	case RunCheckTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0eltyfl, id)

	case Task_1p0v7wv:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1t71n6w, id)

	}
	return nil
}
