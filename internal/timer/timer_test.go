package timer

import (
	"context"
	"gitlab.com/dynamap/dynamap"
	"testing"
	"time"
)

func TestTimerStartEvent(t *testing.T) {
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})

	idg := dynamap.NewMemoryIdGenerator()
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	//engine.StartEventProcessors = append(engine.StartEventProcessors, &TimerEventStarter{})
	tes := &TimerEventStarter{}
	engine.AddStartEventProcessor(func(ctx context.Context, b *dynamap.BPMNEngine) error {
		return tes.StartEventProcesses(ctx, b,
			WithTimerEventStarterData(func() *Data {
				return &Data{Msg: "Hello"}
			}))
	})
	engine.Start()
	defer engine.Stop()

	got := 0
	want := 1
	tries := 0

	for {
		// Check every 100 ms. Start Timer Event should fire after 1 second.
		time.Sleep(100 * time.Millisecond)
		pis, err := history.ReadProcessInstances()
		if err != nil {
			t.Error(err)
		}
		if len(pis) > 0 {
			// Process instance started from timer start event
			pi := pis[0]
			if pi.Status == dynamap.ElementCompleted {
				// Process instance completed
				ti := pi.Data.(*Data)
				got = ti.Count // Count should be incremented in timer script
				break
			}
		}
		if tries == 40 {
			// Something isn't working, don't want test to run forever.
			break
		}
		tries++
	}
	if got != want {
		t.Errorf("got tries %d, want %d", got, want)
	}
}
