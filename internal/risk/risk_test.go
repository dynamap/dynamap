package risk

import (
	dynamap "gitlab.com/dynamap/dynamap"
	"strings"
	"testing"
	"time"
)

func autoCompleteTasks(engine *dynamap.BPMNEngine) {
	for {
		piCmdResp, err := (*engine).SendCommand(dynamap.GetProcessInstancesCmd{})
		if err != nil {
			panic(err)
		}
		piResp := piCmdResp.(dynamap.GetProcessInstancesResp)
		if piResp.Data == nil || len(piResp.Data) == 0 {
			return
		}
		taskCmdResp, _ := (*engine).SendCommand(dynamap.GetTasksCmd{})
		taskResp := taskCmdResp.(dynamap.GetTasksResp)

		for _, t := range taskResp.Data {
			var data any
			if t.Key == "DetermineApprovalTask" {
				data = ApproveTask{
					Approved: true,
					Comments: "Looks good for approval",
				}
			} else {
				data = dynamap.BaseTask{
					CompletedBy: dynamap.CompletedBy{
						CompletedById:   "123",
						CompletedByUser: "John Doe",
					},
					Comments: dynamap.Comments{Comments: "Comments"},
				}
			}
			(*engine).SendCommand(dynamap.CompleteUserTasksCmd{Tasks: []dynamap.CompleteUserTaskCmd{{
				ProcessInstanceId: t.ProcessInstanceId,
				TaskId:            t.Id,
				Data:              data,
			}}})
		}
		time.Sleep(1 * time.Millisecond)
	}
}

func TestRisk90Low(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, &Risk{Score: 90})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "LowRiskTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "MediumRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "HighRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "DetermineApprovalTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.NoneEndEventType, "ApprovedEndEvent", dynamap.ElementCompleted, 1)
}

func TestRisk91Low(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, &Risk{Score: 91})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "LowRiskTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "MediumRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "HighRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "DetermineApprovalTask", dynamap.ElementCompleted, 1)
}

func TestRisk75Medium(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, &Risk{Score: 75})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "LowRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "MediumRiskTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "HighRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "DetermineApprovalTask", dynamap.ElementCompleted, 1)
}

func TestRisk89Medium(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, &Risk{Score: 89})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "LowRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "MediumRiskTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "HighRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "DetermineApprovalTask", dynamap.ElementCompleted, 1)
}

func TestRisk74High(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, &Risk{Score: 74})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "LowRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "MediumRiskTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "HighRiskTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "DetermineApprovalTask", dynamap.ElementCompleted, 1)
}

func TestUnmarshalLowRiskTask(t *testing.T) {
	data := `{
		"tasks": [
			{"processInstanceId":"123", "taskId": "45", "taskKey": "LowRiskTask", "data": {"completedById": "1", "completedByUser": "joe", "comments": "test"}}	
		]
	}`
	cmd, err := UnmarshalTask(strings.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	baseTask := cmd.Tasks[0].Data.(dynamap.BaseTask)
	if baseTask.Comments.Comments != "test" {
		t.Errorf("expected baseTask.Comments.Comments == '%s', got '%s'", "test", baseTask.Comments.Comments)
	}
}

func TestUnmarshalDetermineApprovalTask(t *testing.T) {
	data := `{
		"tasks": [
			{"processInstanceId":"123", "taskId": "45", "taskKey": "DetermineApprovalTask", "data": {"approved": true, "comments": "test"}}	
		]
	}`
	cmd, err := UnmarshalTask(strings.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	appTask := cmd.Tasks[0].Data.(ApproveTask)
	if appTask.Approved != true {
		t.Errorf("expected appTask.Approved == true, got false")
	}

	if appTask.Comments != "test" {
		t.Errorf("expected appTask.Comments == '%s', got '%s'", "test", appTask.Comments)
	}
}
