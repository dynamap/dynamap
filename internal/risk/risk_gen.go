package risk

import (
	"bytes"
	"encoding/json"
	"io"
	"strings"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	RiskLevelSequenceFlow = "RiskLevelSequenceFlow"

	SequenceFlowLow = "SequenceFlowLow"

	SequenceFlowMedium = "SequenceFlowMedium"

	SequenceFlowHigh = "SequenceFlowHigh"

	SequenceFlow_0l5tqx1 = "SequenceFlow_0l5tqx1"

	SequenceFlow_0w7t5n9 = "SequenceFlow_0w7t5n9"

	SequenceFlow_0lbbufh = "SequenceFlow_0lbbufh"

	SequenceFlow_0ws1ehw = "SequenceFlow_0ws1ehw"

	SequenceFlow_11dlpfx = "SequenceFlow_11dlpfx"

	SequenceFlow_1or3ltq = "SequenceFlow_1or3ltq"

	SequenceFlow_1wm77ao = "SequenceFlow_1wm77ao"

	StartEvent_1 = "StartEvent_1"

	ApprovedEndEvent = "ApprovedEndEvent"

	RejectedEndEvent = "RejectedEndEvent"

	ExclusiveGateway_11ubehw = "ExclusiveGateway_11ubehw"

	ExclusiveGateway_0dxnubq = "ExclusiveGateway_0dxnubq"

	ExclusiveGateway_0etqaxv = "ExclusiveGateway_0etqaxv"

	LowRiskTask = "LowRiskTask"

	HighRiskTask = "HighRiskTask"

	MediumRiskTask = "MediumRiskTask"

	DetermineApprovalTask = "DetermineApprovalTask"
)

type RiskProcess struct {
	mu   sync.RWMutex
	risk *Risk

	// callback block
	start func()
	done  func()
}

func NewRiskProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, risk *Risk) *dynamap.ProcessInstance {
	impl := &RiskProcess{
		//			mu: sync.RWMutex{},
		risk:  risk,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "RiskProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *RiskProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.risk
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case LowRiskTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case HighRiskTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case MediumRiskTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case DetermineApprovalTask:
			var taskData ApproveTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *RiskProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case RiskLevelSequenceFlow:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if p.risk.Score >= 90 {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlowLow, id)
			} else if p.risk.Score >= 75 && p.risk.Score < 90 {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlowMedium, id)
			} else if p.risk.Score < 75 {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlowHigh, id)
			}
			panic("No Default Flow!")

			return nil
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_11ubehw, id, gwHandler)

	case SequenceFlowLow:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, LowRiskTask, id, "", true)

	case SequenceFlowMedium:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, MediumRiskTask, id, "", true)

	case SequenceFlowHigh:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, HighRiskTask, id, "", true)

	case SequenceFlow_0l5tqx1:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0w7t5n9, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_0w7t5n9:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[ApproveTask](t.ProcessInstance.Id, t.Id, DetermineApprovalTask, id, "", true)

	case SequenceFlow_0lbbufh:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0w7t5n9, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_0ws1ehw:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0w7t5n9, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_11dlpfx:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if dynamap.TaskData[ApproveTask](engine, DetermineApprovalTask).Approved && strings.Contains(dynamap.TaskData[ApproveTask](engine, DetermineApprovalTask).Comments, "good") {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1or3ltq, id)
			} else {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1wm77ao, id)
			}
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0etqaxv, id, gwHandler)

	case SequenceFlow_1or3ltq:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, ApprovedEndEvent, id, true)

	case SequenceFlow_1wm77ao:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, RejectedEndEvent, id, true)

	case StartEvent_1:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, RiskLevelSequenceFlow, id)

	case ApprovedEndEvent:
		return nil

	case RejectedEndEvent:
		return nil

	case ExclusiveGateway_11ubehw:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0dxnubq:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0etqaxv:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case LowRiskTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0l5tqx1, id)

	case HighRiskTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0ws1ehw, id)

	case MediumRiskTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0lbbufh, id)

	case DetermineApprovalTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_11dlpfx, id)

	}
	return nil
}
