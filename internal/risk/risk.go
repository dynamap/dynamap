package risk

type Risk struct {
	Score int
}

type ApproveTask struct {
	Approved bool
	Comments string
}
