package taskautocompletionprocess

import (
	"gitlab.com/dynamap/dynamap"
	"testing"
)

func TestSubProcess(t *testing.T) {
	var primary, history *dynamap.MemoryStateStore
	primary = dynamap.NewMemoryStateStore()
	history = dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	g := dynamap.NewMemoryIdGenerator()
	engine := dynamap.NewBPMNEngine(g, store, store, store)
	g.Start(engine.Ctx)
	engine.Start()
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewTaskAutoCompletionProcess(id, engine, startEvent, &Data{})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	t.Logf("Waiting...")
	engine.Wait()
	t.Logf("Stopping...")
	engine.Stop()
	t.Logf("Stopped!")
	pInfo, err := history.ReadProcessInstance("P1")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("P1 read!")
	got := pInfo.Data.(*Data).printCount
	want := 3
	if got != want {
		t.Errorf("got %d printCount, want %d", got, want)
	}

	got = pInfo.Data.(*Data).emailCount
	want = 1
	if got != want {
		t.Errorf("got %d emailCount, want %d", got, want)
	}

	got = pInfo.Data.(*Data).completeCount
	want = 2
	if got != want {
		t.Errorf("got %d completeCount, want %d", got, want)
	}
	t.Logf("TestSubProcess complete!")
}
