package taskautocompletionprocess

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	AutomateCompletionSubProcess = "AutomateCompletionSubProcess"

	Flow_0ei5vh5 = "Flow_0ei5vh5"

	Flow_0p28fnz = "Flow_0p28fnz"

	Flow_064m031 = "Flow_064m031"

	Flow_1mp9aw4 = "Flow_1mp9aw4"

	Flow_15t9not = "Flow_15t9not"

	Flow_0gbjemv = "Flow_0gbjemv"

	Event_1v6mesz = "Event_1v6mesz"

	WorkCompleteEndEvent = "WorkCompleteEndEvent"

	WorkNotCompleteEndEvent = "WorkNotCompleteEndEvent"

	Gateway_1kexgz1 = "Gateway_1kexgz1"

	GetWorkStatusScript = "GetWorkStatusScript"

	CompleteTaskScript = "CompleteTaskScript"

	Activity_1ufo4b3 = "Activity_1ufo4b3"

	PrintTasksSubprocess = "PrintTasksSubprocess"

	SequenceFlow_0zv0erj = "SequenceFlow_0zv0erj"

	SequenceFlow_0q8fnjo = "SequenceFlow_0q8fnjo"

	StartEvent_06yi6kx = "StartEvent_06yi6kx"

	EndEvent_0k3ed3n = "EndEvent_0k3ed3n"

	PrintTasksScript = "PrintTasksScript"

	Flow_0yw9g9f = "Flow_0yw9g9f"

	Flow_14fos09 = "Flow_14fos09"

	Flow_04h34yw = "Flow_04h34yw"

	SequenceFlow_0gumub6 = "SequenceFlow_0gumub6"

	StartEvent_1 = "StartEvent_1"

	EndEvent = "EndEvent"

	QueryTasksScript = "QueryTasksScript"
)

type TaskAutoCompletionProcess struct {
	mu   sync.RWMutex
	data *Data

	// callback block
	start func()
	done  func()
}

func NewTaskAutoCompletionProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data *Data) *dynamap.ProcessInstance {
	impl := &TaskAutoCompletionProcess{
		//			mu: sync.RWMutex{},
		data:  data,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "TaskAutoCompletionProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *TaskAutoCompletionProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.data
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *TaskAutoCompletionProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case AutomateCompletionSubProcess:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_04h34yw, id)

	case PrintTasksSubprocess:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0gumub6, id)

	case Flow_0yw9g9f:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.queryTasks()
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, QueryTasksScript, id, script)

	case Flow_14fos09:
		id := g.GenerateId(dynamap.SubProcessType)
		spGetNextElement := func(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider, loopCounter int) dynamap.ElementStateProvider {
			switch currentElement.GetElementState().Key {
			case SequenceFlow_0zv0erj:
				id := g.GenerateId(dynamap.ScriptTaskType)
				script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
					p.data.printTasks()
				}
				return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, PrintTasksScript, id, script)
			case SequenceFlow_0q8fnjo:
				id := g.GenerateId(dynamap.NoneEndEventType)
				return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, EndEvent_0k3ed3n, id, true)
			case StartEvent_06yi6kx:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0zv0erj, id)
			case EndEvent_0k3ed3n:
				return nil
			case PrintTasksScript:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0q8fnjo, id)
			}
			return nil
		}
		return dynamap.NewSubProcess(t.ProcessInstance.Id, t.Id, PrintTasksSubprocess, id, engine, g, StartEvent_06yi6kx, 1, spGetNextElement)

	case Flow_04h34yw:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, EndEvent, id, true)

	case SequenceFlow_0gumub6:
		id := g.GenerateId(dynamap.SubProcessType)
		numberOfInstances := len(p.data.tasks)
		spGetNextElement := func(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider, loopCounter int) dynamap.ElementStateProvider {
			switch currentElement.GetElementState().Key {
			case Flow_0ei5vh5:
				id := g.GenerateId(dynamap.ScriptTaskType)
				script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
					fmt.Printf("Get work status %s\n", p.data.tasks[loopCounter].Name)
				}
				return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, GetWorkStatusScript, id, script)
			case Flow_0p28fnz:
				id := g.GenerateId(dynamap.ExclusiveGatewayType)
				gwHandler := func() dynamap.ElementStateProvider {
					p.mu.RLock()
					defer p.mu.RUnlock()
					if p.data.tasks[loopCounter].WorkDone == true {
						id := g.GenerateId(dynamap.SequenceFlowType)
						return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_064m031, id)
					} else {
						id := g.GenerateId(dynamap.SequenceFlowType)
						return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_15t9not, id)
					}
				}
				return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, Gateway_1kexgz1, id, gwHandler)
			case Flow_064m031:
				id := g.GenerateId(dynamap.ScriptTaskType)
				script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
					fmt.Printf("Complete task %s\n", p.data.tasks[loopCounter].Name)
					p.data.completeCount++
				}
				return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, CompleteTaskScript, id, script)
			case Flow_1mp9aw4:
				id := g.GenerateId(dynamap.NoneEndEventType)
				return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, WorkCompleteEndEvent, id, true)
			case Flow_15t9not:
				id := g.GenerateId(dynamap.ScriptTaskType)
				script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
					fmt.Printf("Emailing work not complete for task %s\n", p.data.tasks[loopCounter].Name)
					p.data.emailCount++
				}
				return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, Activity_1ufo4b3, id, script)
			case Flow_0gbjemv:
				id := g.GenerateId(dynamap.NoneEndEventType)
				return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, WorkNotCompleteEndEvent, id, true)
			case Event_1v6mesz:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0ei5vh5, id)
			case WorkCompleteEndEvent:
				return nil
			case WorkNotCompleteEndEvent:
				return nil
			case Gateway_1kexgz1:
				gw := currentElement.(*dynamap.ExclusiveGateway)
				return gw.NextElement
			case GetWorkStatusScript:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0p28fnz, id)
			case CompleteTaskScript:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1mp9aw4, id)
			case Activity_1ufo4b3:
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0gbjemv, id)
			}
			return nil
		}
		return dynamap.NewSubProcess(t.ProcessInstance.Id, t.Id, AutomateCompletionSubProcess, id, engine, g, Event_1v6mesz, numberOfInstances, spGetNextElement)

	case StartEvent_1:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0yw9g9f, id)

	case EndEvent:
		return nil

	case QueryTasksScript:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_14fos09, id)

	}
	return nil
}
