package taskautocompletionprocess

import "fmt"

type Data struct {
	tasks                                 []Task
	printCount, completeCount, emailCount int
}

func (d *Data) queryTasks() {
	d.tasks = []Task{
		Task{"Task A", true},
		Task{"Task B", false},
		Task{"Task C", true},
	}
}

func (d *Data) printTasks() {
	for _, t := range d.tasks {
		fmt.Println(t.Name)
		d.printCount++
	}
}

type Task struct {
	Name     string
	WorkDone bool
}
