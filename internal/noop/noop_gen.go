package noop

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	SequenceFlow_0b2hds0 = "SequenceFlow_0b2hds0"

	SequenceFlow_1v0nuva = "SequenceFlow_1v0nuva"

	StartEvent_1 = "StartEvent_1"

	PlanningEndEvent = "PlanningEndEvent"

	Task_00hzm9m = "Task_00hzm9m"
)

type NoOpProcess struct {
	mu   sync.RWMutex
	data *Data

	// callback block
	start func()
	done  func()
}

func NewNoOpProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data *Data) *dynamap.ProcessInstance {
	impl := &NoOpProcess{
		//			mu: sync.RWMutex{},
		data:  data,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "NoOpProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *NoOpProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.data
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *NoOpProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case SequenceFlow_0b2hds0:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, Task_00hzm9m, id, script)

	case SequenceFlow_1v0nuva:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, PlanningEndEvent, id, true)

	case StartEvent_1:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0b2hds0, id)

	case PlanningEndEvent:
		return nil

	case Task_00hzm9m:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1v0nuva, id)

	}
	return nil
}
