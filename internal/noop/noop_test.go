package noop

import (
	"fmt"
	"gitlab.com/dynamap/dynamap"
	"log"
	"runtime"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestManualBenchmarkNoop(t *testing.T) {
	fmt.Printf("GOMAXPROCS: %d\n", runtime.GOMAXPROCS(0))
	const count = 100000
	engine := dynamap.NewMemoryBPMNEngine()
	engine.Start()
	begin := time.Now()
	for n := 0; n < count; n++ {
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		pi := NewNoOpProcess(id, engine, startEvent, &Data{})
		//engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
		engine.RunProcessInstanceToCompletion(pi)
	}
	engine.Wait()
	engine.Stop()
	completed := time.Now()
	fmt.Printf("Complete %d instances time: %v\n", count, completed.Sub(begin))
	fmt.Printf("PIs completed/second: %f\n", 1.0/((float64(completed.Sub(begin).Milliseconds()))/count/1000.0))
	piCount := engine.ProcessInstanceCount()
	if piCount != 0 {
		t.Fatalf("Expected 0 process instances, got %d", piCount)
	}
}

func TestManualBenchmarkNoOpPool(t *testing.T) {
	const count = 100000
	MAX_PROCS := runtime.GOMAXPROCS(0)
	fmt.Printf("GOMAXPROCS: %d\n", runtime.GOMAXPROCS(0))
	chmap := make(map[int]chan *dynamap.ProcessInstance)
	engmap := make(map[int]*dynamap.BPMNEngine)

	wg := sync.WaitGroup{}
	wg.Add(MAX_PROCS)
	begin := time.Now()
	for i := 0; i < MAX_PROCS; i++ {
		ch := make(chan *dynamap.ProcessInstance)
		chmap[i] = ch
		engine := dynamap.NewMemoryBPMNEngine()
		engine.Start()
		engmap[i] = engine
		go func(ch <-chan *dynamap.ProcessInstance, engine *dynamap.BPMNEngine) {
			defer func() {
				engine.Wait()
				engine.Stop()
				wg.Done()
			}()
			for {
				select {
				case pi, ok := <-ch:
					if !ok {
						return
					}
					engine.RunProcessInstanceToCompletion(pi)
				}
			}
		}(ch, engine)
	}
	for n := 0; n < count; n++ {
		var gr int
		if n != 0 {
			gr = n % MAX_PROCS
		}
		ch, ok := chmap[gr]
		if !ok {
			log.Fatalf("cannot lookup in map: i %d, gr %d", n, gr)
		}
		engine := engmap[gr]
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		pi := NewNoOpProcess(id, engine, startEvent, &Data{})
		ch <- pi
	}
	for _, ch := range chmap {
		close(ch)
	}
	wg.Wait()
	completed := time.Now()
	fmt.Printf("Complete %d instances time: %v\n", count, completed.Sub(begin))
	fmt.Printf("PIs completed/second: %f\n", 1.0/((float64(completed.Sub(begin).Milliseconds()))/count/1000.0))
}

func BenchmarkNoOp(b *testing.B) {
	b.Logf("b.N: %d", b.N)
	engine := dynamap.NewMemoryBPMNEngine()
	engine.Start()
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		p := NewNoOpProcess(id, engine, startEvent, &Data{})
		engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	}
	engine.Wait()
	engine.Stop()
}
