package plan

type Plan struct {
	Fusion bool
	Score  int
}

type UpdatePlanCmd struct {
	ProcessInstanceId string
	Plan
}

func (c *UpdatePlanCmd) Id() string {
	return c.ProcessInstanceId
}

type GetPlanCmd struct {
	ProcessInstanceId string
}

func (c *GetPlanCmd) Id() string {
	return c.ProcessInstanceId
}

func (p *StartPlanProcess) ExecuteCmd(cmd any) any {
	switch v := cmd.(type) {
	case UpdatePlanCmd:
		p.mu.Lock()
		defer p.mu.Unlock()
		p.plan.Fusion = v.Fusion
		p.plan.Score = v.Score
		return p.plan
	case GetPlanCmd:
		return p.plan
	}
	return nil
}
