package plan

import (
	"context"
	"fmt"
	"gitlab.com/dynamap/dynamap"
)

type ProcessInstanceStoreStartupProcessor struct {
	engine *dynamap.BPMNEngine
}

func (s *ProcessInstanceStoreStartupProcessor) StartEventProcesses(ctx context.Context) {
	store := s.engine.StateStore

	// Load Process Instances
	storePis, err := store.ReadProcessInstances()
	if err != nil {
		return
	}
	pis := make([]*dynamap.ProcessInstance, len(storePis))
	pisMap := make(map[string]*dynamap.ProcessInstance, len(storePis))
	for i := range storePis {
		spi := storePis[i]
		plan, ok := spi.Data.(*Plan)
		if !ok {
			fmt.Println("Could not load plan data. Ignoring PI.")
			continue
		}
		impl := StartPlanProcess{plan: plan}
		pi := dynamap.ProcessInstance{BpmnEngine: s.engine, Id: spi.Id, Key: spi.Key, Version: spi.Version, Created: spi.Created, Status: spi.Status, Impl: &impl}
		pi.Tokens = make([]*dynamap.Token, 0)
		pis[i] = &pi
		pisMap[pi.Id] = &pi
	}

	// Load Tokens
	storeTokens, err := store.ReadTokens()
	if err != nil {
		return
	}
	for i := range storeTokens {
		st := storeTokens[i]
		pi := pisMap[st.ProcessInstanceId]
		token := dynamap.NewToken(nil, pi, s.engine)
		token.Id = st.Id
		pi.Tokens = append(pi.Tokens, token)
	}

	// Load Elements
	storeElements, err := store.ReadAllElementStates()
	for i := range storeElements {
		se := storeElements[i]
		pi := pisMap[se.ProcessInstanceId]
		var t *dynamap.Token
		for j := range pi.Tokens {
			if pi.Tokens[j].Id == se.TokenId {
				t = pi.Tokens[j]
			}
		}
		var element dynamap.ElementStateProvider
		if se.ElementType == dynamap.UserTaskType {
			element = dynamap.NewUserTask[dynamap.BaseTask](pi.Id, se.TokenId, se.Key, se.Id, se.Status, true)
		}
		if element == nil {
			fmt.Printf("warning, unhandled element type: %+v\n", se)
			continue
		}
		if t == nil {
			fmt.Printf("warning, token not found for element: %+v\n", se)
			continue
		}
		t.CurrentElement = element

	}

	// Todo: Check all PIS have tokens filled out correctly...
	for i := range pis {
		pi := pis[i]
		delTokens := make([]*dynamap.Token, 0)
		for j := range pi.Tokens {
			t := pi.Tokens[j]
			if t.CurrentElement == nil || t.IsComplete() {
				delTokens = append(delTokens, t)
			}
		}
		for j := range delTokens {
			pi.RemoveToken(delTokens[j])
		}
	}

	// Start process instances
	for i := range pis {
		_, err := s.engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: pis[i]})
		if err != nil {
			fmt.Println(err)
		}
	}
}
