package plan

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	SequenceFlow_1rgyt8e = "SequenceFlow_1rgyt8e"

	SequenceFlow_0hbc8s4 = "SequenceFlow_0hbc8s4"

	SequenceFlow_04cepq3 = "SequenceFlow_04cepq3"

	SequenceFlow_0tnr42a = "SequenceFlow_0tnr42a"

	SequenceFlow_0zwsz2i = "SequenceFlow_0zwsz2i"

	SequenceFlow_1fdvgyu = "SequenceFlow_1fdvgyu"

	SequenceFlow_11ovoow = "SequenceFlow_11ovoow"

	SequenceFlow_0dpgu80 = "SequenceFlow_0dpgu80"

	SequenceFlow_1tated2 = "SequenceFlow_1tated2"

	SequenceFlow_0tu9bqm = "SequenceFlow_0tu9bqm"

	SequenceFlow_0ibwzxj = "SequenceFlow_0ibwzxj"

	StartEvent_1 = "StartEvent_1"

	PlanningEndEvent = "PlanningEndEvent"

	ExclusiveGateway_1clbvj4 = "ExclusiveGateway_1clbvj4"

	ExclusiveGateway_0k2bk5r = "ExclusiveGateway_0k2bk5r"

	ParallelGateway_09iq80z = "ParallelGateway_09iq80z"

	ParallelGateway_06j37h3 = "ParallelGateway_06j37h3"

	FusionTask = "FusionTask"

	ContouringTask = "ContouringTask"

	MDContouringTask = "MDContouringTask"

	CreatePlanScriptTask = "CreatePlanScriptTask"
)

type StartPlanProcess struct {
	mu   sync.RWMutex
	plan *Plan

	// callback block
	start func()
	done  func()
}

func NewStartPlanProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, plan *Plan) *dynamap.ProcessInstance {
	impl := &StartPlanProcess{
		//			mu: sync.RWMutex{},
		plan:  plan,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "StartPlanProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *StartPlanProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.plan
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case FusionTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case ContouringTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case MDContouringTask:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *StartPlanProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case SequenceFlow_1rgyt8e:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if !p.plan.Fusion {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0tnr42a, id)
			} else {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0hbc8s4, id)
			}
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_1clbvj4, id, gwHandler)

	case SequenceFlow_0hbc8s4:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, FusionTask, id, "", true)

	case SequenceFlow_04cepq3:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0zwsz2i, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0k2bk5r, id, gwHandler)

	case SequenceFlow_0tnr42a:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			id := g.GenerateId(dynamap.SequenceFlowType)
			return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0zwsz2i, id)
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ExclusiveGateway_0k2bk5r, id, gwHandler)

	case SequenceFlow_0zwsz2i:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0zwsz2i}
		outSeqKeys := []string{SequenceFlow_1fdvgyu, SequenceFlow_11ovoow}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_09iq80z, id, SequenceFlow_0zwsz2i, inSeqKeys, outSeqKeys)

	case SequenceFlow_1fdvgyu:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, ContouringTask, id, "", true)

	case SequenceFlow_11ovoow:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[dynamap.BaseTask](t.ProcessInstance.Id, t.Id, MDContouringTask, id, "", true)

	case SequenceFlow_0dpgu80:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0dpgu80, SequenceFlow_1tated2}
		outSeqKeys := []string{SequenceFlow_0tu9bqm}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_06j37h3, id, SequenceFlow_0dpgu80, inSeqKeys, outSeqKeys)

	case SequenceFlow_1tated2:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0dpgu80, SequenceFlow_1tated2}
		outSeqKeys := []string{SequenceFlow_0tu9bqm}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_06j37h3, id, SequenceFlow_1tated2, inSeqKeys, outSeqKeys)

	case SequenceFlow_0tu9bqm:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			fmt.Sprintln("Created Plan")
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, CreatePlanScriptTask, id, script)

	case SequenceFlow_0ibwzxj:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, PlanningEndEvent, id, true)

	case StartEvent_1:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1rgyt8e, id)

	case PlanningEndEvent:
		return nil

	case ExclusiveGateway_1clbvj4:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0k2bk5r:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case ParallelGateway_09iq80z:
		return nil

	case ParallelGateway_06j37h3:
		return nil

	case FusionTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_04cepq3, id)

	case ContouringTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0dpgu80, id)

	case MDContouringTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1tated2, id)

	case CreatePlanScriptTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0ibwzxj, id)

	}
	return nil
}
