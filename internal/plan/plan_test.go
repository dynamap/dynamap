package plan

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/dynamap/dynamap"
	"strconv"
	"testing"
	"time"
)

func autoCompleteTasks(engine *dynamap.BPMNEngine, num int, ready chan<- any) {
	sid, ch := engine.StateStore.SubscribeElementState()
	defer engine.StateStore.UnsubscribeElementState(sid)
	ready <- struct{}{}
	count := 0
	for {
		var taskState dynamap.ElementState
		select {
		case taskState = <-ch:
			if taskState.ElementType == dynamap.UserTaskType && taskState.Status == dynamap.ElementActivated {
				// todo: task send could happen before task is in receiving state? So it tries to complette
				(*engine).SendCommand(dynamap.CompleteUserTasksCmd{Tasks: []dynamap.CompleteUserTaskCmd{{
					ProcessInstanceId: taskState.ProcessInstanceId,
					TaskId:            taskState.Id,
					Data: dynamap.BaseTask{
						CompletedBy: dynamap.CompletedBy{
							CompletedById:   "123",
							CompletedByUser: "John Doe",
						},
						Comments: dynamap.Comments{Comments: "Comments"}},
				}}})
			} else if taskState.ElementType == dynamap.NoneEndEventType && taskState.Status == dynamap.ElementCompleted {
				//fmt.Println("NoneEndEvent")
				count += 1
				if count == num {
					return
				}
			}
		case <-time.After(1 * time.Millisecond):
			resp, err := (*engine).SendCommand(dynamap.GetTasksCmd{})
			if err != nil {
				if errors.Is(err, dynamap.ErrBPMNEngineCancelled) {
					return
				}
				fmt.Printf("error getting tasks: %v\n", err)
				return
			}
			taskRespCmd := resp.(dynamap.GetTasksResp)
			completeTasks := make([]dynamap.CompleteUserTaskCmd, 0)
			for _, t := range taskRespCmd.Data {
				completeTasks = append(completeTasks, dynamap.CompleteUserTaskCmd{
					ProcessInstanceId: t.ProcessInstanceId,
					TaskId:            t.Id,
					Data: dynamap.BaseTask{
						CompletedBy: dynamap.CompletedBy{
							CompletedById:   "123",
							CompletedByUser: "John Doe",
						},
						Comments: dynamap.Comments{Comments: "Comments"}},
				})
			}
			_, err = (*engine).SendCommand(dynamap.CompleteUserTasksCmd{Tasks: completeTasks})
			if err != nil {
				fmt.Printf("autocomplete error2: %v\n", err)
			}
			continue
		}
	}
}

func TestPlanFusion(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	ready := make(chan any)
	go autoCompleteTasks(engine, 1, ready)
	<-ready
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewStartPlanProcess(id, engine, startEvent, &Plan{Fusion: true})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "FusionTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "ContouringTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "MDContouringTask", dynamap.ElementCompleted, 1)
}

func TestPlanNoFusion(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	ready := make(chan any)
	go autoCompleteTasks(engine, 1, ready)
	<-ready
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewStartPlanProcess(id, engine, startEvent, &Plan{Fusion: false})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "FusionTask", dynamap.ElementCompleted, 0)
	checkState(dynamap.UserTaskType, "ContouringTask", dynamap.ElementCompleted, 1)
	checkState(dynamap.UserTaskType, "MDContouringTask", dynamap.ElementCompleted, 1)
}

func TestTwoPlans(t *testing.T) {
	idg := dynamap.NewMemoryIdGenerator()
	primary := dynamap.NewMemoryStateStore()
	history := dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	engine := dynamap.NewBPMNEngine(idg, store, store, store)
	idg.Start(engine.Ctx)
	engine.Start()
	defer engine.Stop()
	ready := make(chan any)
	count := 1000
	go autoCompleteTasks(engine, 2*count, ready)
	<-ready
	for i := 0; i < count; i++ {
		pid := "P" + strconv.Itoa(i)
		startEvent := dynamap.NewNoneStartEvent(pid, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		p := NewStartPlanProcess(pid, engine, startEvent, &Plan{Fusion: false})
		engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
		pid2 := pid + "Fusion"
		startEvent = dynamap.NewNoneStartEvent(pid2, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		p = NewStartPlanProcess(pid2, engine, startEvent, &Plan{Fusion: true})
		engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	}

	engine.Wait()
	for history.ProcessInstanceCount() != 2*count {
		time.Sleep(10 * time.Millisecond)
	}
	checkState := func(elementType string, key string, status string, count int) {
		states, _ := history.ReadElementStates(elementType, key)
		got := 0
		for _, h := range states {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(dynamap.UserTaskType, "FusionTask", dynamap.ElementCompleted, count)
	checkState(dynamap.UserTaskType, "ContouringTask", dynamap.ElementCompleted, 2*count)
	checkState(dynamap.UserTaskType, "MDContouringTask", dynamap.ElementCompleted, 2*count)
	checkState(dynamap.NoneEndEventType, "PlanningEndEvent", dynamap.ElementCompleted, 2*count)
	pis, err := history.ReadProcessInstances()
	if err != nil {
		t.Fatal(err)
	}
	if len(pis) != 2*count {
		t.Errorf("Got '%d' pis, want '%d'", len(pis), 2*count)
	}
}

func TestUpdatePlanCmd(t *testing.T) {
	engine := dynamap.NewMemoryBPMNEngine()
	engine.Start()
	startEvent := dynamap.NewNoneStartEvent("P1", "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewStartPlanProcess("P1", engine, startEvent, &Plan{Fusion: false})
	resp, err := engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	if err != nil {
		t.Fatalf("got unexpected error: %s", err)
	}
	startResp := resp.(dynamap.StartProcessInstanceResp)
	if startResp.Err != nil {
		t.Fatalf("got unexpected error: %s", startResp.Err)
	}
	result, err := engine.SendCommand(dynamap.ProcessInstanceCmd{Id: "P1", Cmd: UpdatePlanCmd{Plan: Plan{Fusion: true, Score: 2}}})
	if err != nil {
		t.Fatalf("Received error sending UpdatePlanCmd: %s", err)
	}
	if err, ok := result.(dynamap.BPMNEngineError); ok {
		t.Fatalf("Received error sending UpdatePlanCmd: %+v", err)
	}

	if result.(*Plan).Fusion != true {
		t.Fatalf("Got fusion false, wanted true")
	}
	result, err = engine.SendCommand(dynamap.ProcessInstanceCmd{Id: "P1", Cmd: GetPlanCmd{}})
	if err != nil {
		t.Fatalf("Received error sending GetPlanCmd")
	}
	plan := result.(*Plan)
	if !plan.Fusion {
		t.Fatalf("Got fusion false, wanted true.")
	}
	got := plan.Score
	want := 2
	if got != want {
		t.Fatalf("Got score %d, want %d", got, want)
	}
}

func TestManualBenchmark(t *testing.T) {
	engine := dynamap.NewMemoryBPMNEngine()
	engine.Start()
	const count = 1000
	begin := time.Now()
	ready := make(chan any)
	go autoCompleteTasks(engine, count, ready)
	<-ready
	for n := 0; n < count; n++ {
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		p := NewStartPlanProcess(id, engine, startEvent, &Plan{Fusion: false})
		engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	}
	processesStarted := time.Now()
	fmt.Printf("Processes started at %v: \n", processesStarted)
	engine.Wait()
	engine.Stop()
	completed := time.Now()
	fmt.Printf("Start %d instances time: %v\n", count, processesStarted.Sub(begin))
	fmt.Printf("Complete %d instances time: %v\n", count, completed.Sub(begin))
	fmt.Printf("PIs completed/second: %f\n", 1.0/((float64(completed.Sub(begin).Milliseconds()))/count/1000.0))
	piCount := engine.ProcessInstanceCount()
	if piCount != 0 {
		t.Fatalf("Expected 0 process instances, got %d", piCount)
	}
}

func TestPlanShutdown(t *testing.T) {
	var primary, history *dynamap.MemoryStateStore
	primary = dynamap.NewMemoryStateStore()
	history = dynamap.NewMemoryStateStore()
	history.StoreCompleted = true
	store := dynamap.NewPrimarySecondaryStateStore(primary, []dynamap.StateStore{history})
	g := dynamap.NewMemoryIdGenerator()

	// Start Engine
	engine := dynamap.NewBPMNEngine(g, store, store, store)
	engine.ProcessorStartNum = 0
	g.Start(engine.Ctx)
	engine.Start()

	// Start Process
	id := "P1"
	startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
	p := NewStartPlanProcess(id, engine, startEvent, &Plan{Fusion: false})
	engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})

	waitForTaskActivation(engine, 2)

	// Terminate process early
	p.Shutdown()
	engine.Wait()

	// Prepare autocompletion of tasks
	ready := make(chan any)
	go autoCompleteTasks(engine, 1, ready)
	<-ready

	// Start terminated process from store
	starter := ProcessInstanceStoreStartupProcessor{engine: engine}
	starter.StartEventProcesses(context.Background())

	// Wait until completion
	waitForPICompletion(engine)
	engine.Wait()
	engine.Stop()

	// Check states
	pInfo, err := history.ReadProcessInstance("P1")
	if err != nil {
		t.Fatalf("unexpected error reading process instance: %v", err)
	}
	got := pInfo.Status
	want := dynamap.ElementCompleted
	if got != want {
		t.Fatalf("got process instance status '%s', want  '%s'", got, want)
	}

	states, err := history.ReadAllElementStates()
	if err != nil {
		t.Fatalf("unexpected error reading elements: %v", err)
	}
	for _, s := range states {
		got := s.Status
		want := dynamap.ElementCompleted
		if got != want {
			t.Errorf("%s %s got status '%s', want '%s'", s.Id, s.Key, got, want)
		}
	}
}

func waitForTaskActivation(engine *dynamap.BPMNEngine, numTasks int) {
	ch := make(chan any)
	go func() {
		sid, ech := engine.StateStore.SubscribeElementState()
		defer engine.StateStore.UnsubscribeElementState(sid)
		taskCount := 0
		for {
			var taskState dynamap.ElementState
			select {
			case taskState = <-ech:
			case <-time.After(10 * time.Second):
				panic("complete task: timeout receiving from subscription\n")
				continue
			}
			if taskState.ElementType == dynamap.UserTaskType && taskState.Status == dynamap.ElementActivated {
				taskCount++
			}
			if taskCount == numTasks {
				ch <- struct{}{}
				return
			}
		}
	}()

	// Wait for tasks to be activated
	<-ch
}

func waitForPICompletion(engine *dynamap.BPMNEngine) {
	ch := make(chan any)
	go func() {
		sid, ech := engine.StateStore.SubscribeElementState()
		defer engine.StateStore.UnsubscribeElementState(sid)
		for {
			var taskState dynamap.ElementState
			select {
			case taskState = <-ech:
			case <-time.After(10 * time.Second):
				panic("pi completion: timeout receiving from subscription\n")
				continue
			}
			if taskState.ElementType == dynamap.NoneEndEventType && taskState.Status == dynamap.ElementCompleted {
				ch <- struct{}{}
				return
			}
		}
	}()

	// Wait for process completion
	<-ch
}

func BenchmarkPlanMemoryStore(b *testing.B) {
	b.Logf("b.N: %d", b.N)
	engine := dynamap.NewMemoryBPMNEngine()
	engine.Start()
	ready := make(chan any)
	go autoCompleteTasks(engine, b.N, ready)
	<-ready
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "StartEvent_1", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		p := NewStartPlanProcess(id, engine, startEvent, &Plan{Fusion: false})
		engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	}
	engine.Wait()
	engine.Stop()
}
