package typical

import (
	"fmt"
	"gitlab.com/dynamap/dynamap"
	"log"
	"runtime"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestManualBenchmarkTypicalPool(t *testing.T) {
	const count = 100000
	MAX_PROCS := runtime.GOMAXPROCS(0)
	fmt.Printf("GOMAXPROCS: %d\n", runtime.GOMAXPROCS(0))
	chmap := make(map[int]chan *dynamap.ProcessInstance)
	engmap := make(map[int]*dynamap.BPMNEngine)

	wg := sync.WaitGroup{}
	wg.Add(MAX_PROCS)
	begin := time.Now()
	for i := 0; i < MAX_PROCS; i++ {
		ch := make(chan *dynamap.ProcessInstance)
		chmap[i] = ch
		engine := dynamap.NewMemoryBPMNEngine()
		engine.Start()
		engmap[i] = engine
		go func(ch <-chan *dynamap.ProcessInstance, engine *dynamap.BPMNEngine) {
			defer func() {
				engine.Wait()
				engine.Stop()
				wg.Done()
			}()
			for {
				select {
				case pi, ok := <-ch:
					if !ok {
						return
					}
					engine.RunProcessInstanceToCompletion(pi)
				}
			}
		}(ch, engine)
	}
	for n := 0; n < count; n++ {
		var gr int
		if n != 0 {
			gr = n % MAX_PROCS
		}
		ch, ok := chmap[gr]
		if !ok {
			log.Fatalf("cannot lookup in map: i %d, gr %d", n, gr)
		}
		engine := engmap[gr]
		id := "P" + strconv.Itoa(n)
		startEvent := dynamap.NewNoneStartEvent(id, "", "start", engine.IdGenerator.GenerateId(dynamap.NoneStartEventType))
		pi := NewTypicalBenchmarkProcess(id, engine, startEvent, &Data{Proceed: true})
		ch <- pi
	}
	for _, ch := range chmap {
		close(ch)
	}
	wg.Wait()
	completed := time.Now()
	fmt.Printf("Complete %d instances time: %v\n", count, completed.Sub(begin))
	fmt.Printf("PIs completed/second: %f\n", 1.0/((float64(completed.Sub(begin).Milliseconds()))/count/1000.0))
}
