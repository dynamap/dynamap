package typical

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	SequenceFlow_1swotv4 = "SequenceFlow_1swotv4"

	SequenceFlow_19e406m = "SequenceFlow_19e406m"

	SequenceFlow_1fcsq3j = "SequenceFlow_1fcsq3j"

	SequenceFlow_1ktyxza = "SequenceFlow_1ktyxza"

	SequenceFlow_0h8p3qy = "SequenceFlow_0h8p3qy"

	SequenceFlow_0pvrpnr = "SequenceFlow_0pvrpnr"

	SequenceFlow_11i3dru = "SequenceFlow_11i3dru"

	SequenceFlow_02ln9k8 = "SequenceFlow_02ln9k8"

	SequenceFlow_18f5tlt = "SequenceFlow_18f5tlt"

	SequenceFlow_1ofj7hs = "SequenceFlow_1ofj7hs"

	Flow_0z2tud8 = "Flow_0z2tud8"

	Flow_11ozwsi = "Flow_11ozwsi"

	Flow_13nehq4 = "Flow_13nehq4"

	start = "start"

	end = "end"

	Event_1bcf2he = "Event_1bcf2he"

	Gateway_09jrxib = "Gateway_09jrxib"

	task1 = "task1"

	task2 = "task2"

	task3 = "task3"

	task4 = "task4"

	task5 = "task5"

	task6 = "task6"

	task7 = "task7"

	task8 = "task8"

	task9 = "task9"

	task10 = "task10"
)

type TypicalBenchmarkProcess struct {
	mu   sync.RWMutex
	data *Data

	// callback block
	start func()
	done  func()
}

func NewTypicalBenchmarkProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data *Data) *dynamap.ProcessInstance {
	impl := &TypicalBenchmarkProcess{
		//			mu: sync.RWMutex{},
		data:  data,
		start: func() {},
		done:  func() {},
	}
	return &dynamap.ProcessInstance{
		Key:          "TypicalBenchmarkProcess",
		Id:           id,
		BpmnEngine:   bpmnEngine,
		Status:       dynamap.ElementCreating,
		Created:      time.Now(),
		Version:      "1",
		StartElement: startElement,
		Impl:         impl,
	}
}

// ProcessInstanceData - implements interface ProcessInstanceDataRetriever
func (p *TypicalBenchmarkProcess) ProcessInstanceData() any {
	//    p.mu.Lock()
	//    defer p.mu.Unlock()
	return p.data
}

func UnmarshalTask(rdr io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{
		Tasks: make([]dynamap.CompleteUserTaskCmd, 0),
	}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(rdr).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func (p *TypicalBenchmarkProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case SequenceFlow_1swotv4:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task1, id, script)

	case SequenceFlow_19e406m:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task3, id, script)

	case SequenceFlow_1fcsq3j:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if p.data.Proceed == true {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0z2tud8, id)
			} else {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_11ozwsi, id)
			}
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, Gateway_09jrxib, id, gwHandler)

	case SequenceFlow_1ktyxza:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task5, id, script)

	case SequenceFlow_0h8p3qy:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task6, id, script)

	case SequenceFlow_0pvrpnr:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task7, id, script)

	case SequenceFlow_11i3dru:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task8, id, script)

	case SequenceFlow_02ln9k8:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task9, id, script)

	case SequenceFlow_18f5tlt:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task10, id, script)

	case SequenceFlow_1ofj7hs:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, end, id, true)

	case Flow_0z2tud8:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task4, id, script)

	case Flow_11ozwsi:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, Event_1bcf2he, id, true)

	case Flow_13nehq4:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.Count += 1
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, task2, id, script)

	case start:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1swotv4, id)

	case end:
		return nil

	case Event_1bcf2he:
		return nil

	case Gateway_09jrxib:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case task1:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_13nehq4, id)

	case task2:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_19e406m, id)

	case task3:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1fcsq3j, id)

	case task4:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1ktyxza, id)

	case task5:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0h8p3qy, id)

	case task6:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0pvrpnr, id)

	case task7:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_11i3dru, id)

	case task8:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_02ln9k8, id)

	case task9:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_18f5tlt, id)

	case task10:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1ofj7hs, id)

	}
	return nil
}
