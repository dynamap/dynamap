package dynamap

import "context"

// StartEvent represents a BPMN none start event.
type StartEvent struct {
	ElementState
}

// NewNoneStartEvent creates a new none start event.
func NewNoneStartEvent(pid, tid, key, id string) *StartEvent {
	return &StartEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: NoneStartEventType, Status: ElementActivated}}
}

// GetElementState returns the state of the StartEvent instance
func (e *StartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *StartEvent) RefElement() any {
	return e
}

func (e *StartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	return nil
}
