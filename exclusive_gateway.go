package dynamap

import "context"

type ExclusiveGateway struct {
	ElementState
	Handler     func() ElementStateProvider
	NextElement ElementStateProvider
}

func NewExclusiveGateway(pid, tid, key, id string, handler func() ElementStateProvider) *ExclusiveGateway {
	return &ExclusiveGateway{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: ExclusiveGatewayType}, Handler: handler}
}

func (eg *ExclusiveGateway) GetElementState() ElementState {
	return eg.ElementState
}

func (eg *ExclusiveGateway) RefElement() any {
	return eg
}

func (eg *ExclusiveGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	eg.Completing(ctx, bpmnEngine, token)
	eg.Completed(ctx, bpmnEngine, token)
	return nil
}

func (eg *ExclusiveGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleting
	if eg.Store {
		bpmnEngine.WriteElement(eg.ElementState)
	}
	eg.NextElement = eg.Handler()
}

func (eg *ExclusiveGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleted
	if eg.Store {
		bpmnEngine.WriteElement(eg.ElementState)
	}
}
