# Generate all internal workflow test code
generate:
	cd internal/noop && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
	cd internal/plan && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
	cd internal/risk && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
	cd internal/timer && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
	cd internal/typical && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap
	cd internal/subprocess && go run gitlab.com/dynamap/dynamap/builder/cmd/dynamap

# Clear test cache and run tests with race condition checking and a timeout
test:
	GOFLAGS="-count=1" go test -race -v ./...
#	GOFLAGS="-count=1" go test -race -v -timeout 60s ./...
