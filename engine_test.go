package dynamap

import (
	"strings"
	"testing"
)

func TestBaseBPMNEngine_SendCommand_NotStarted(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	result, err := engine.SendCommand("test")
	if result != nil {
		t.Fatalf("Got engine send command result %v, expected nil", result)
	}
	if err.Error() != ErrBPMNEngineNotStarted.Error() {
		t.Fatalf("Got error %v, expected %v", err.Error(), ErrBPMNEngineNotStarted.Error())
	}
}

func TestBaseBPMNEngine_SendCommand_Cancelled(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	engine.Start()
	engine.Stop()
	result, err := engine.SendCommand("test")
	if result != nil {
		t.Fatalf("Got engine send command result %v, expected nil", result)
	}
	bpmnError, ok := err.(BPMNEngineError)
	if !(ok && strings.HasPrefix(bpmnError.Message, ErrBPMNEngineCancelled.Message)) {
		t.Fatalf("Got error %v, expected %v", err.Error(), ErrBPMNEngineCancelled.Error())
	}
}

func TestBaseBPMNEngine_GenerateIdNotSet(t *testing.T) {
	engine := BPMNEngine{}
	err := engine.Start()
	if err == nil || err.Error() != ErrBPMNEngineIdGeneratorNotSet.Error() {
		t.Fatalf("Got %v error, expected %v", err, ErrBPMNEngineIdGeneratorNotSet.Error())
	}
}

func TestBaseBPMNEngine_StateStoreNotSet(t *testing.T) {
	s := NewMemoryStateStore()
	engine := BPMNEngine{IdGenerator: NewMemoryIdGenerator(), ProcessInstanceStore: s, TokenStore: s}
	err := engine.Start()
	if err == nil || err.Error() != ErrBPMNEngineStateStoreNotSet.Error() {
		t.Fatalf("Got %v error, expected %v", err, ErrBPMNEngineStateStoreNotSet.Error())
	}
}

func TestBaseBPMNEngine_Start(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	err := engine.Start()
	if err != nil {
		t.Fatalf("Got error %v, expected nil", err)
	}
	if engine.Ctx == nil {
		t.Fatalf("engine.ctx is nil!")
	}
	if engine.processInstances == nil {
		t.Fatalf("engine.processInstances is nil!")
	}
	if engine.cmdReq == nil {
		t.Fatalf("engine.cmdReq is nil!")
	}
	if engine.stateStream == nil {
		t.Fatalf("engine.stateStream is nil!")
	}
	engine.Stop()
}

func TestBaseBPMNEngine_GetProcessInstances(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	engine.Start()
	defer engine.Stop()
	for i := 0; i < 50; i++ {
		// Test no errors requesting process instances when none exist
		_, err := engine.SendCommand(GetProcessInstancesCmd{})
		if err != nil {
			t.Fatalf("Got error %v, expected nil", err)
			return
		}
	}
	engine.Wait()
}
