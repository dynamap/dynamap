package dynamap

import "context"

type SendTask struct {
	ElementState
}

func (s *SendTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	s.Status = ElementCompleted
	if s.Store {
		bpmnEngine.WriteElement(s.ElementState)
	}
	return nil
}
