package dynamap

import (
	"context"
	"fmt"
	"log"
	"time"
)

type BaseTask struct {
	CompletedBy
	Comments
}

type CompletedBy struct {
	CompletedById   string
	CompletedByUser string
}

type Comments struct {
	Comments string
}

type UserTask[T any] struct {
	ElementState
	completed     bool
	CompletedTime time.Time
	Data          T
}

type GetUserTaskCmd struct {
}

type CompleteUserTaskCmd struct {
	ProcessInstanceId string
	TaskId            string
	Data              any
}

type CompleteUserTaskResp struct {
	ProcessInstanceId string
	TaskId            string
	Error             error
}

func NewUserTask[T any](pid, tid, key, id, status string, store bool) *UserTask[T] {
	task := UserTask[T]{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: UserTaskType, Status: status, Store: store}}
	task.Object = &task
	return &task
}

func (t *UserTask[T]) GetElementState() ElementState {
	return t.ElementState
}

func (t *UserTask[T]) RefElement() any {
	return t
}

func (t *UserTask[T]) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	if err := t.Activated(ctx, bpmnEngine, token); err != nil {
		return err
	}
	t.Completed(ctx, bpmnEngine, token)
	return nil
}

func (t *UserTask[T]) Activated(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	t.Status = ElementActivated
	if t.Store {
		bpmnEngine.WriteElement(t.ElementState)
	}
	return t.RunTask(ctx, bpmnEngine, token)
}

func (t *UserTask[T]) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	t.Status = ElementCompleted
	if t.Store {
		bpmnEngine.WriteElement(t.ElementState)
	}
}

func (t *UserTask[T]) RunTask(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	token.setAcceptingCommands(true)
	defer func() {
		token.setAcceptingCommands(false)
	}()
	for {
		//fmt.Printf("task wait: %+v\n", t)
		select {
		case <-ctx.Done():
			return fmt.Errorf("usertask %s cancelled", t.Key)
		case <-time.After(10 * time.Second):
			//fmt.Printf("task rx time out: %+v\n", t)
		case cmd := <-token.cmdReq:
			switch c := cmd.(type) {
			case CompleteUserTaskCmd:
				resp := CompleteUserTaskResp{token.ProcessInstance.Id, t.Id, nil}
				if c.TaskId == t.Id && c.ProcessInstanceId == token.ProcessInstance.Id {
					data, ok := c.Data.(T)
					if ok {
						// Task Complete
						t.completed = true
						t.CompletedTime = time.Now()
						t.Data = data
						token.cmdResp <- resp
						return nil
					} else {
						err := fmt.Errorf("unable to marshal data to type %T", t.Data)
						resp.Error = NewBPMNEngineError(err, "completeUserTaskCmd error")
						log.Println(resp.Error)
					}
				} else {
					err := fmt.Errorf("got processId %s and taskId %s, want processId %s, taskId %s", c.ProcessInstanceId, c.TaskId, token.ProcessInstance.Id, t.Id)
					resp.Error = NewBPMNEngineError(err, "completeUserTaskCmd error")
				}
				token.cmdResp <- resp
			case GetUserTaskCmd:
				pi := token.ProcessInstance
				token.cmdResp <- TaskState{
					ProcessInstanceKey:     pi.Key,
					ProcessInstanceId:      pi.Id,
					ProcessInstanceVersion: pi.Version,
					Key:                    t.Key,
					Id:                     t.Id,
					Status:                 t.Status,
					Data:                   t.Data, // Todo: Copy data
				}
			}
		}
	}
}

func (t *UserTask[T]) Clone() *UserTask[T] {
	newTask := UserTask[T]{ElementState: ElementState{
		Key:         t.Key,
		Id:          t.Id,
		ElementType: t.ElementType,
		Status:      t.Status,
		//Created:     t.Created, //TODO: ADD
	},
		completed:     t.completed,
		CompletedTime: t.CompletedTime,
	}

	return &newTask
}
