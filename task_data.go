package dynamap

// TaskData return data for the given task indexed by key
func TaskData[T any](engine *BPMNEngine, key string) T {
	states, err := engine.StateStore.ReadElementStates(UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*UserTask[T]).Data
}
