package dynamap

// helper functions for starting process instances
// possibly rename to something better ??

import (
	"context"
)

type NewInstance[T any] func(string, *BPMNEngine, ElementStateProvider, T) *ProcessInstance

type InstanceStarter[T any] func(context.Context, T) error

// NewInstanceStarter create a process intsance starter function
// this create a None event - maybe use None in the name
func NewInstanceStarter[T any](engine *BPMNEngine, newInstance NewInstance[T], eventName string) InstanceStarter[T] {
	return func(ctx context.Context, data T) error {
		processId := engine.IdGenerator.GenerateId(ProcessInstanceType)
		event := NewNoneStartEvent(processId, "", eventName, engine.IdGenerator.GenerateId(NoneStartEventType))
		instance := newInstance(processId, engine, event, data)
		_, err := engine.SendCommand(StartProcessInstanceCmd{Instance: instance})
		return err
	}
}

// StartInstanceWithChannel - use a channel to receive a give payload on a channel
// and start a process instance
func StartInstanceWithChannel[T any](ctx context.Context, c chan T, startInstance InstanceStarter[T]) StartEventProcessor {
	return func(ctx context.Context, b *BPMNEngine) error {
		go func() {
			for {
				select {
				case <-ctx.Done():
					return
				case datum := <-c:
					startInstance(ctx, datum)
				}
			}
		}()
		return nil
	}
	return nil
}
