package dynamap

import (
	"context"
	"time"
)

type TimerDelayOpt time.Duration

// ClockSource interface for externally clocked timer start processes
type ClockSource interface {
	SpecHandler(spec string, handler func()) error
	Start()
	Stop()
}

type TimerStartEvent struct {
	ElementState
}

func NewTimerStartEvent(pid, tid, key, id string) *TimerStartEvent {
	return &TimerStartEvent{ElementState: ElementState{ProcessInstanceId: pid, TokenId: tid, Key: key, Id: id, ElementType: TimerStartEventType}}
}

func (e *TimerStartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *TimerStartEvent) RefElement() any {
	return e
}

func (e *TimerStartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) error {
	e.Status = ElementCompleted
	return nil
}
