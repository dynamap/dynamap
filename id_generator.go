package dynamap

import (
	"context"
	"strconv"
	"sync/atomic"
)

type IdGenerator interface {
	GenerateId(elementType string) string
}

type MemoryIdGenerator struct {
	id uint64
}

func NewMemoryIdGenerator() *MemoryIdGenerator {
	return &MemoryIdGenerator{}
}

func (g *MemoryIdGenerator) Start(ctx context.Context) {

}

func (g *MemoryIdGenerator) GenerateId(_ string) string {
	return strconv.Itoa(int(atomic.AddUint64(&g.id, 1)))
}
